#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
useradd -m -U -G sudo -s /bin/bash cadre
echo "cadre:password" | chpasswd
echo "root:password" | chpasswd
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
systemctl restart ssh
apt-get update -y
apt-get -y install locate make dnsutils lsof aptitude auditd git zip unzip figlet lolcat sshpass hexedit libncurses5-dev ncurses-hexedit gcc
apt-get -y install build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf
apt-get -y install curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev netcat nmap apache2 tree rename
apt-get -y install libqtdbus4 qtchooser libqt4-xml qdbus libqt4-dbus qtcore4-l10n libqtcore4 libqtgui4
apt-get -yq upgrade
gem install lolcat
updatedb
mandb
mkdir -p /opt/trusted_tools/networking
wget https://git.cybbh.space/net/public/raw/master/modules/networking/heat/packages_and_scripts/Cisco-PT-7.1.1x64.tar?inline=false -O /opt/trusted_tools/networking/Cisco-PT-7.1.1x64.tar
cd /opt/trusted_tools/networking && tar xvf Cisco-PT-7.1.1x64.tar
rm install

cat > ./install.sh <<"__PT__"
#!/bin/bash
# Thanks to Brent C., Ruben L., Alan C. for improving this install script to install, unassisted.
# Thanks to Paul Fedele for providing script to check/download 32-bit library on a 64-bit machine
echo
echo "Welcome to Cisco Packet Tracer 7.1.1 Installation"
echo
installer ()
{
SDIR=$(dirname $_)
echo "Packet Tracer will now be installed in the default location [/opt/pt]"
IDIR="/opt/pt"
if [ -e $IDIR ]; then
  rm -rf $IDIR
fi
QIDIR=${IDIR//\//\\\\\/}
echo "Installing into $IDIR"
if mkdir $IDIR > /dev/null 2>&1; then
  if cp -r $SDIR/* $IDIR; then
    echo "Copied all files successfully to $IDIR"
  fi
  sh -c "sed s%III%$IDIR% $SDIR/tpl.packettracer > $IDIR/packettracer"
  chmod +x $IDIR/packettracer
  sh -c "sed s%III%$IDIR% $SDIR/tpl.linguist > $IDIR/linguist"
  chmod +x $IDIR/linguist
  if touch /usr/share/applications/pt7.desktop > /dev/null 2>&1; then
    echo -e "[Desktop Entry]\nExec=PacketTracer7\nIcon=pt7\nType=Application\nTerminal=false\nName=Packet Tracer 7.1" | tee /usr/share/applications/pt7.desktop > /dev/null
    rm -f /usr/share/icons/hicolor/48x48/apps/pt7.png
    gtk-update-icon-cache -f -q /usr/share/icons/hicolor
    sleep 10
    cp $SDIR/art/app.png /usr/share/icons/hicolor/48x48/apps/pt7.png
    gtk-update-icon-cache -f -q /usr/share/icons/hicolor
  fi
else
  echo
  if mkdir -p $IDIR; then
    echo "Installing into $IDIR"
      if cp -r $SDIR/* $IDIR; then
         echo "Copied all files successfully to $IDIR"
      else
        echo
        echo "Not able to copy files to $IDIR"
        echo "Exiting installation"
        exit
      fi
    sh -c "sed 's/III/$QIDIR/ $SDIR/tpl.packettracer > $IDIR/packettracer'"
    chmod +x $IDIR/packettracer
    sh -c "sed 's/III/$QIDIR/ $SDIR/tpl.linguist > $IDIR/linguist'"
    chmod a+x $IDIR/linguist
      if touch /usr/share/applications/pt7.desktop; then
        echo -e "[Desktop Entry]\nExec=PacketTracer7\nIcon=pt7\nType=Application\nTerminal=false\nName=Packet Tracer 7.1" | tee /usr/share/applications/pt7.desktop > /dev/null
        rm -f /usr/share/icons/hicolor/48x48/apps/pt7.png
        gtk-update-icon-cache -f -q /usr/share/icons/hicolor
        sleep 10
        cp $SDIR/art/app.png /usr/share/icons/hicolor/48x48/apps/pt7.png
        gtk-update-icon-cache -f -q /usr/share/icons/hicolor
      fi
  else
    echo
    echo "Not able to gain root access with sudo"
    echo "Exiting installation"
    exit
  fi
fi
echo
ln -sf $IDIR/packettracer /usr/local/bin/packettracer
echo "Type \"packettracer\" in a terminal to start Cisco Packet Tracer"
# add the environment var PT7HOME
sh -c sed 's/sudo//g' -i /opt/pt/set_ptenv.sh
sh -c sed 's/sudo//g' -i /opt/pt/set_qtenv.sh
chmod +x /opt/pt/set_*
sh -c /opt/pt/set_ptenv.sh $IDIR
sh -c /opt/pt/set_qtenv.sh
echo
echo "Cisco Packet Tracer 7.1.1 installed successfully"
echo "Please restart you computer for the Packet Tracer settings to take effect"
}
installer
exit 0
__PT__
chmod +x install.sh
./install.sh

wget https://gitlab.com/D4NP0UL1N/Public/-/raw/master/linux/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb?inline=false -O /usr/lib/x86_64-linux-gnu/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb
dpkg -i /usr/lib/x86_64-linux-gnu/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb
apt-get -y install libqt5webkit5 libqt5multimedia5 libqt5printsupport5 libqt5script5 libqt5scripttools5


cat <<EOF > packettracer
#!/bin/bash
echo "Starting Packet Tracer 7.1.1"
PTDIR=/opt/pt
#export LD_LIBRARY_PATH=\$PTDIR/lib
pushd \$PTDIR/bin > /dev/null
./PacketTracer7 "\$@" > /dev/null 2>&1 &
popd > /dev/null
EOF
chmod +x packettracer
ln -s /opt/trusted_tools/networking/packettracer /usr/local/bin/packettracer

cat > /etc/motd <<"__MOTD__"
 ******************************************************************
*                                                                   *
*  This system is for the use of authorized users only.  Usage of   *
*  this system may be monitored and recorded by system personnel.   *
*                                                                   *
*  Anyone using this system expressly consents to such monitoring   *
*  and is advised that if such monitoring reveals possible          *
*  evidence of criminal activity, system personnel may provide the  *
*  evidence from such monitoring to law enforcement officials.      *
*                                                                   *
 ******************************************************************
__MOTD__

echo 'echo "Packet Tracer" | figlet | lolcat' >> /root/.bashrc
hostnamectl set-hostname 'Packet-Tracer'
hostname Packet-Tracer
reboot

