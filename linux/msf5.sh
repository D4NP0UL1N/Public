#MSF Install (Postgresql, database, and msfupdate)
# DO NOT run this script outright, as it will fail ..
# paste sections at a time .. follow instructions.

#RUBY INSTALL

cd ~
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc

# sudo plugin so we can run Metasploit as root with "rbenv sudo msfconsole"
 
git clone git://github.com/dcarley/rbenv-sudo.git ~/.rbenv/plugins/rbenv-sudo

exec $SHELL
RUBYVERSION=$(wget https://raw.githubusercontent.com/rapid7/metasploit-framework/master/.ruby-version -q -O - )
rbenv install $RUBYVERSION
sleep 5
rbenv global $RUBYVERSION
ruby -v

#NMAP INSTALL

mkdir ~/Development
cd ~/Development
git clone https://github.com/nmap/nmap.git
cd nmap 
./configure
make
make install
make clean

#CONFIG POSTGRESQL SERVER
cd /usr/share
sudo -s
su postgres
createuser msf -P -S -R -D
#ISSUE HERE: createuser requires password input
createdb -O msf msf 
exit

#INSTALLING MSF
cd /opt
sudo git clone https://github.com/rapid7/metasploit-framework.git
sudo chown -R `whoami` /opt/metasploit-framework
cd metasploit-framework

#INSTALLING REQUIRED GEMS AND VERS
gem install bundler
bundle install

sudo bash -c 'for MSF in $(ls msf*); do ln -s /opt/metasploit-framework/$MSF /usr/local/bin/$MSF;done'

touch /opt/metasploit-framework/config/database.yml
echo "production:">/opt/metasploit-framework/config/database.yml
echo " adapter: postgresql">>/opt/metasploit-framework/config/database.yml
echo " database: msf ">>/opt/metasploit-framework/config/database.yml
echo " username: msf ">>/opt/metasploit-framework/config/database.yml
echo " password: password">>/opt/metasploit-framework/config/database.yml
echo " host: 127.0.0.1">>/opt/metasploit-framework/config/database.yml
echo " port: 5432">>/opt/metasploit-framework/config/database.yml
echo " pool: 75">>/opt/metasploit-framework/config/database.yml
echo " timeout: 5">>/opt/metasploit-framework/config/database.yml

sudo sh -c "echo export MSF_DATABASE_CONFIG=/opt/metasploit-framework/config/database.yml >> /etc/profile"
source /etc/profile

service postgresql start
update-rc.d postgresql enable
bash

msfconsole -q
msf > db_status
#	[*] postgresql connected to msf
# msf> db_rebuild_cache
# db_rebuild takes about 15 minutes to complete
