# To fix Metasploit Database:

# Run the following commands sequentially (read all before attempting):
service postgresql start
cd /usr/share
sudo -s
su postgres
#    Adjust next command with your Red Team Number:
createuser red_team_1 -P -S -R -D
#    password input: p@ssw0rd
#    feel free to change password above, BUT also change one below in database.yml
createdb -O red_team_1 red_team_1
#  IF you make a mistake, the database WILL NOT let you use the same database name again; pick a new one ..
#  OR, you can remove it using this syntax: dropuser red_team_1
exit


# Adds all Metasploit executables to your $PATH ; [COPY] + [PASTE] into your terminal:
sudo bash -c 'for MSF in $(ls msf*); do ln -s /usr/share/metasploit-framework/$MSF /usr/local/bin/$MSF;done'

# Creates Metasploit database file for connecting to the postgresql database:
# [COPY] + [PASTE] ALL below (from [cat down to EOL]) into your terminal, and hit [ENTER]:
# Adjust "database" and "username" to your Red Team Number.
cat > /usr/share/metasploit-framework/config/database.yml <<"EOL"

production:                                               
 adapter: postgresql                                                
 database: red_team_1
 username: red_team_1                                          
 password: p@ssw0rd                                               
 host: 127.0.0.1                                                
 port: 5432                                                
 pool: 75                                               
 timeout: 5   
 
 EOL

 # That's it!  If you type:
 #  ~# msfconsole -q
 #  msf> db_status

 #     You should be connected to your assigned database.
 #     Now you can import NESSUS, OpenVAS, Nmap, etc. scan data (xml format).
 #     Using syntax:  msf> db_import file.xml
