# Check for all user challenge.txt files: 
# gci C:\Users\*\Desktop -recurse  | Select Length, FullName | findstr /i challenge

$ErrorActionPreference = 'SilentlyContinue'

#----- Req. for Invoke-WebRequest to pull from https://GitLab
[System.Net.ServicePointManager]::SecurityProtocol = @("TLS12","TLS11","TLS","SSL3")

#----- LOCK OUT Administrator from that which was set on yaml / Instructor ACCESS ONLY ---
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####    ---   LOCK OUT USERS FROM ADMIN ACCESS  ---       ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""

net user Administrator ReallyStrongPassword!!
Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\winlogon" -Name "DefaultPassword" -PropertyType String -Value 'ReallyStrongPassword!!'
Update-Help -Force

#----- OUs ---
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####         ---   SETUP ACTIVE DIRECTORY OUs  ---        ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
New-ADOrganizationalUnit -Name CTF -Path "DC=cyber,DC=warriors"
New-ADOrganizationalUnit -Name Alpha -Path "OU=CTF,DC=cyber,DC=warriors"
New-ADOrganizationalUnit -Name Bravo -Path "OU=CTF,DC=cyber,DC=warriors"
New-ADOrganizationalUnit -Name Charlie -Path "OU=CTF,DC=cyber,DC=warriors"
New-ADOrganizationalUnit -Name Delta -Path "OU=CTF,DC=cyber,DC=warriors"

#----- User Security Groups ---
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####         ---  SETUP USER SECURITY GROUPS   ---        ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
New-ADGroup -Name Alphas -Path "CN=Users,DC=cyber,DC=warriors" -GroupCategory Security -GroupScope Universal
 
New-ADGroup -Name Bravos -Path "CN=Users,DC=cyber,DC=warriors" -GroupCategory Security -GroupScope Global 
Add-ADGroupMember -Identity "CN=Bravos,CN=Users,DC=cyber,DC=warriors" -Members "CN=Alphas,CN=Users,DC=cyber,DC=warriors"

New-ADGroup -Name Charlies -Path "CN=Users,DC=cyber,DC=warriors" -GroupCategory Security -GroupScope Global 
Add-ADGroupMember -Identity "CN=Charlies,CN=Users,DC=cyber,DC=warriors" -Members "CN=Bravos,CN=Users,DC=cyber,DC=warriors"

New-ADGroup -Name Deltas -Path "CN=Users,DC=cyber,DC=warriors" -GroupCategory Security -GroupScope Global 
Add-ADGroupMember -Identity "CN=Deltas,CN=Users,DC=cyber,DC=warriors" -Members "CN=Charlies,CN=Users,DC=cyber,DC=warriors"

#----- Share Drive Setup ---
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####             ---  SHARE DRIVE SETUP   ---             ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
#----- Creates Share Folders for all Levels ---  10990 folders
Set-Variable -Name ROOT -Value "C:\share"
Set-Variable -Name TOP -Value "CTF"
New-Item -ItemType Directory -Path "$ROOT\$TOP\" -Force

$CLASSES = @("Alpha","Bravo","Charlie","Delta")
$1STFOLDER = @("1","2","3","4","5","6","7","8","9","10")
$2NDFOLDER = @("10","9","8","7","6","5","4","3","2","1")
$3RDFOLDER = @("1","2","3","4","5","6","7","8","9","10")
$4THFOLDER = @("10","9","8","7","6","5","4","3","2","1")

foreach ($CLASS in $CLASSES) {
	Write-Output $CLASS
	foreach ($3RD in $3RDFOLDER) {
		Write-Output $3RD
		foreach ($2ND in $2NDFOLDER) {
			Write-Output $2ND
			foreach ($1ST in $1STFOLDER) {
				New-Item -ItemType Directory -Path "$ROOT\$TOP\$CLASS\$3RD\HOME\$2ND\HOME\$1ST" -Force
			}
		}
	}
}
    foreach ($4TH in $4THFOLDER) {
        Write-Output $4TH
        foreach ($3RD in $3RDFOLDER) {
            New-Item -ItemType Directory -Path "$CLASS\$1ST\$4TH\$3RD"
            }
        }
start-sleep -s 1
	
# ----- creates SMB share for folders created above ---
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####               ---  SETUP SMB SHARE   ---             ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
New-SMBshare `
-Path "C:\share" `
-Name "The Share" `
-Desc "A Share Drive"

# Deprecated Command / doesn't seem to work in PowerShell Core:
# Add-NTFSAccess -Path C:\share -Account 'Everyone' -AccessRights Read

#----- Grant User Groups File Share READ Access:
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####     ---  GRANT GROUPS SHARE READ ACCESS   ---        ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
icacls "C:\share\CTF" /grant Everyone:R /C
icacls "C:\share\CTF\Deltas" /grant Deltas:R /T /C
icacls "C:\share\CTF\Charlies" /grant Charlies:R /T /C
icacls "C:\share\CTF\Bravos" /grant Bravos:R /T /C
icacls "C:\share\CTF\Alphas" /grant Alphas:R /T /C

#----- DISABLES PASSWORD COMPLEXITY REQ AND ENABLES LOCAL LOGIN FOR USERS OTHER THAN ADMIN ---
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####     ---  DISABLE PASSWORD COMPLEXITY REQS   ---      ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
secedit /export /cfg c:\secpol.cfg
(gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
(gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
(gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
rm -force c:\secpol.cfg -confirm:$false

New-ADFineGrainedPasswordPolicy `
-Name "DomainUsersPSO" `
-Precedence 10 `
-ComplexityEnabled $false `
-Description "The Domain Users Password Policy" `
-DisplayName "Domain Users PSO" `
-LockoutDuration "0.00:00:10" `
-LockoutObservationWindow "0.00:00:05" `
-LockoutThreshold 10 `
-MaxPasswordAge "60.00:00:00" `
-MinPasswordAge "1.00:00:00" `
-MinPasswordLength 1 `
-PasswordHistoryCount 24 `
-ReversibleEncryptionEnabled $false
Add-ADFineGrainedPasswordPolicySubject DomainUsersPSO -Subjects 'Domain Users'

#----- Not Working: Disables Control Panel, Registry.exe GUIs, and Last User Name at login ---
#Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System" -Name DontDisplayLastUserName -Value 1

#----- Sets Password -----
# PowerShell Core fails to utilize this method for PSExec command further down; use cmd / 'For ..' instead
# Import-Module 'Microsoft.PowerShell.Security'
# $pass = ConvertTo-SecureString -String 'password' -AsPlainText -Force
# $expire = $((Get-Date).AddDays(180)) 


#----- Creates 50 Domain User Accounts .. 3 hidden ---  all must have default password to be used in follow on psexec for loops
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####        ---  CREATE IND. USER ACCOUNTS   ---          ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
$users1 = @("Alpha01","Alpha02","Alpha03","Alpha04","Alpha05","Alpha06","Alpha07","Alpha08","Alpha09","Alpha10")
foreach ($user in $users1) {
New-ADUser -Name $user -Path "OU=Alpha,OU=CTF,DC=cyber,DC=warriors" -AccountPassword $pass -ChangePasswordAtLogon $false -CannotChangePassword $false -Passwordneverexpires $false -AccountExpirationDate $expire -Enabled $true -AllowReversiblePasswordEncryption $false 
Add-ADGroupMember -Identity "CN=Alphas,CN=Users,DC=cyber,DC=warriors" -Members "CN=$user,OU=Alpha,OU=CTF,DC=cyber,DC=warriors"
}
$users2 = @("Bravo01","Bravo02","Bravo03","Bravo04","Bravo05","Bravo06","Bravo07","Bravo08","Bravo09","Bravo10")
foreach ($user in $users2) {
New-ADUser -Name $user -Path "OU=Bravo,OU=CTF,DC=cyber,DC=warriors" -AccountPassword $pass -ChangePasswordAtLogon $false -CannotChangePassword $false -Passwordneverexpires $false -AccountExpirationDate $expire -Enabled $true -AllowReversiblePasswordEncryption $false
Add-ADGroupMember -Identity "CN=Bravos,CN=Users,DC=cyber,DC=warriors" -Members "CN=$user,OU=Bravo,OU=CTF,DC=cyber,DC=warriors"
}
$users3 = @("Charlie01","Charlie02","Charlie03","Charlie04","Charlie05","Charlie06","Charlie07","Charlie08","Charlie09","Charlie10")
foreach ($user in $users3) {
New-ADUser -Name $user -Path "OU=Charlie,OU=CTF,DC=cyber,DC=warriors" -AccountPassword $pass -ChangePasswordAtLogon $false -CannotChangePassword $false -Passwordneverexpires $false -AccountExpirationDate $expire -Enabled $true -AllowReversiblePasswordEncryption $false 
Add-ADGroupMember -Identity "CN=Charlies,CN=Users,DC=cyber,DC=warriors" -Members "CN=$user,OU=Charlie,OU=CTF,DC=cyber,DC=warriors"
}
$users4 = @("Delta01","Delta02","Delta03","Delta04")
foreach ($user in $users4) {
New-ADUser -Name $user -Path "OU=Delta,OU=CTF,DC=cyber,DC=warriors" -AccountPassword $pass -ChangePasswordAtLogon $false -CannotChangePassword $false -Passwordneverexpires $false -AccountExpirationDate $expire -Enabled $true -AllowReversiblePasswordEncryption $false 
Add-ADGroupMember -Identity "CN=Deltas,CN=Users,DC=cyber,DC=warriors" -Members "CN=$user,OU=Delta,OU=CTF,DC=cyber,DC=warriors"
}

#----- Creates Profiles for Every User/Level in Domain .. to be populated with the follow-on challenges
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " #### -  CREATE USER LIST - FOR PASSWORDS AND ICACLS  -    ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
$users = @'
Alpha01
Alpha02
Alpha03
Alpha04
Alpha05
Alpha06
Alpha07
Alpha08
Alpha09
Alpha10
Bravo01
Bravo02
Bravo03
Bravo04
Bravo05
Bravo06
Bravo07
Bravo08
Bravo09
Bravo10
Charlie01
Charlie02
Charlie03
Charlie04
Charlie05
Charlie06
Charlie07
Charlie08
Charlie09
Charlie10
Delta01
Delta02
Delta03
Delta04
'@
echo $users > C:\Windows\System32\users.txt

# Set all user passwords to default "password":
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####   ---  SET USER ACCOUNT PASSWORDS DEFAULT   ---      ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
cmd /c "For /F %a IN ('type C:\Windows\System32\users.txt') DO net user %a password /Y"

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####  ---  CREATE USER ACCOUNT FOLDER STRUCTURE   ---     ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
$users1 = @("Alpha01","Alpha02","Alpha03","Alpha04","Alpha05","Alpha06","Alpha07","Alpha08","Alpha09","Alpha10")
foreach ($user in $users1) {
	psexec.exe -accepteula -u ctf\$user -p password \\$(hostname) cmd /c "exit" -accepteula -nobanner
	start-sleep -s 1
}
$users2 = @("Bravo01","Bravo02","Bravo03","Bravo04","Bravo05","Bravo06","Bravo07","Bravo08","Bravo09","Bravo10")
foreach ($user in $users2) {
	psexec.exe -accepteula -u ctf\$user -p password \\$(hostname) cmd /c "exit" -accepteula -nobanner
}
$users3 = @("Charlie01","Charlie02","Charlie03","Charlie04","Charlie05","Charlie06","Charlie07","Charlie08","Charlie09","Charlie10")
foreach ($user in $users3) {
	psexec.exe -accepteula -u ctf\$user -p password \\$(hostname) cmd /c "exit" -accepteula -nobanner
}
$users4 = @("Delta01","Delta02","Delta03","Delta04")
foreach ($user in $users4) {
	psexec.exe -accepteula -u ctf\$user -p password \\$(hostname) cmd /c "exit" -accepteula -nobanner
}

#----- Specific Files for each account/level/challenge .. modifies domain user accounts with correct challenge "passwords"
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####   ---  SETTING UP - ALPHA - USER CHALLENGES  ---     ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""

Set-ADAccountPassword -Identity Alpha01 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "password" -Force)
	Write-Output "The password for the next level is the Powershell build version. Please be sure to include all periods" -n > C:\Users\Alpha01\Desktop\challenge.txt

Set-ADAccountPassword -Identity Alpha02 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$(($PSVersionTable.BuildVersion).ToString())" -Force)
	Write-Output "The password for the next level is the short name of the domain in which this server is a part of." -n > C:\Users\Alpha02\Desktop\challenge.txt

Set-ADAccountPassword -Identity Alpha03 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "cyber" -Force)
	Write-Output "The password for the next level is in a readme file somewhere in this user’s profile." -n > C:\Users\Alpha03\Desktop\challenge.txt
	
Set-ADAccountPassword -Identity Alpha04 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "123456" -Force)
    New-Item -ItemType File -Path "C:\Users\Alpha04\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is in a file in a hidden directory in the root of this user’s profile." -n > C:\Users\Alpha04\Desktop\challenge.txt
	Write-Output "123456" > C:\Users\Alpha03\Favorites\README

Set-ADAccountPassword -Identity Alpha05 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "ketchup" -Force)
    New-Item -ItemType File -Path "C:\Users\Alpha05\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is in a file in a directory on the desktop with spaces in it." -n > C:\Users\Alpha05\Desktop\challenge.txt
	New-Item -ItemType Directory -Path "C:\Users\Alpha04\secretsauce" -Force
	Write-Output "ketchup" > C:\Users\Alpha04\secretsauce\saucey
	attrib +h C:\Users\Alpha04\secretsauce

Set-ADAccountPassword -Identity Alpha06 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "987654321" -Force)
	Write-Output "The password for the next level is the manufacturing name of the only USB drive that was plugged into this server at some point." -n > C:\Users\Alpha06\Desktop\challenge.txt
	$dirs = @("1    -     99","100     -     199","a     -      z","z                                                                                                           -                                                            a")
	foreach ($dir in $dirs) {
		New-Item -ItemType Directory -Path C:\Users\Alpha05\Desktop\$dir -Force
		}
	Write-Output "987654321" > "C:\Users\Alpha05\Desktop\z                                                                                                           -                                                            a\space.txt"

Set-ADAccountPassword -Identity Alpha07 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "SanDisk" -Force)
	Write-Output "The password for the next level is the description of the Lego Land service." -n > C:\Users\Alpha07\Desktop\challenge.txt
	Set-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Services\USBSTOR" -Name Start -Value 1 
	New-Item "C:\windows\system32" -ItemType File -Name reg.ps1 -Force
		Write-Output 'New-Item "HKLM:\SYSTEM\CurrentControlSet\Enum" -Name USBSTOR -Force' > "C:\windows\system32\reg.ps1"
			Write-Output 'New-Item "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Device Parameters" -Force' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Capabilities" -Value "0X00000010" -PropertyType DWord | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Class" -Value "DiskDrive" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "ClassGUID" -Value "{4d36e967-e325-11ce-bfc1-08002be10318}" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "CompatibleIDs" -Value "USBSTOR\Disk USBSTOR\RAW" -PropertyType MultiString | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "ConfigFlags" -Value "0X00000000" -PropertyType DWord | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "ContainerID" -Value "{c2dc3c42-a281-557a-a6ed-e607894e99b3}" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "DeviceDesc" -Value "@disk.inf;%disk_devdesc%;Disk Drive" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Driver" -Value "{4d36e967-e325-11ce-bfc1-08002be10318}\0001" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "FriendlyName" -Value "SanDisk Cruzer Blade USB Device" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "HardwareID" -Value "USBSTOR\DiskSanDisk_Cruzer_Blade___PMAP USBST..." -PropertyType MultiString | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Mfg" -Value "@disk.inf;%genmanufacturer%;Standard disk drives" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Service" -Value "disk" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
				Write-Output 'New-Item "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0\Device Parameters" -Name "MediaChangeNotification" -Force' >> "C:\windows\system32\reg.ps1"
				Write-Output 'New-Item "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0\Device Parameters" -Name "Partmgr" -Force' >> "C:\windows\system32\reg.ps1"
					Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0\Device Parameters\Partmgr" -Name "Attributes" -Value "0X00000000" -PropertyType DWord | Out-Null' >> "C:\windows\system32\reg.ps1"
					Write-Output 'New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0\Device Parameters\Partmgr" -Name "DiskId" -Value "{116c15b5-5f04-11e5-9d2b-000c293089ea}" -PropertyType String | Out-Null' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-Item "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "LogConf" -Force' >> "C:\windows\system32\reg.ps1"
			Write-Output 'New-Item "HKLM:\SYSTEM\CurrentControlSet\Enum\USBSTOR\Disk_Ven_SanDisk_Prod_Cruzer_Blade_Rev_PMAP\CF52A6CB0" -Name "Properties" -Force' >> "C:\windows\system32\reg.ps1"
		schtasks /create /tn "Reg" /tr "powershell.exe -file C:\windows\system32\reg.ps1" /ru SYSTEM /sc ONCE /st (get-date).AddMinutes(1).ToString("HH:mm") /V1 /z
		# Register-ScheduledJob -Name USB -FilePath  C:\windows\system32\reg.ps1 -RunNow
	    start-sleep -s 1

Set-ADAccountPassword -Identity Alpha08 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "i_love_legos" -Force)
        New-Item -ItemType File -Path "C:\Users\Alpha08\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of files in the Videos folder." -n > C:\Users\Alpha08\Desktop\challenge.txt
	New-Service LegoLand -Desc "i_love_legos" "C:\windows\system32\notepad.exe"
	# Hint: HKLM\SYSTEM\CurrentControlSet\Services

Set-ADAccountPassword -Identity Alpha09 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "925" -Force)
        New-Item -ItemType File -Path "C:\Users\Alpha09\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of folders in the Music folder." -n > C:\Users\Alpha09\Desktop\challenge.txt
	0..698 | % { New-Item -ItemType File -Path C:\Users\Alpha08\Videos\file$_.txt -Force }
	710..776 | % { New-Item -ItemType File -Path C:\Users\Alpha08\Videos\file$_.txt -Force }
	834..991 | % { New-Item -ItemType File -Path C:\Users\Alpha08\Videos\file$_.txt -Force }
	New-Item -ItemType Directory -Path "C:\Users\Alpha08\Videos" -Force
	New-Item -ItemType File -Path "C:\Users\Alpha08\Videos\file1103.txt" -Force


Set-ADAccountPassword -Identity Alpha10 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "411" -Force)
        New-Item -ItemType File -Path "C:\Users\Alpha10\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of words in a file on the desktop." -n > C:\Users\Alpha10\Desktop\challenge.txt
	Write-Output "Note: Next Level Login - Bravo01" -n >> C:\Users\Alpha10\Desktop\challenge.txt
	1..703 | % {if($_ % 2 -eq 1 ) { New-Item -ItemType Directory -Path C:\Users\Alpha09\Music\Stevie_Wonder$_ -Force } }
	18..73 | % { New-Item -ItemType Directory -Path C:\Users\Alpha09\Music\Teddy_Pendergrass$_ -Force }
	New-Item -ItemType Directory -Path "C:\Users\Alpha09\Music\Teddy_Pendergrass" -Force
	New-Item -ItemType Directory -Path "C:\Users\Alpha09\Music\Luther Vandros" -Force
	New-Item -ItemType Directory -Path "C:\Users\Alpha09\Music\Stevie_Wonder 139" -Force

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####   ---  SETTING UP - BRAVO - USER CHALLENGES  ---     ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
Set-ADAccountPassword -Identity Bravo01 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "5254" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo01\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the last five digits of the MD5 hash of the hosts file." -n > C:\Users\Bravo01\Desktop\challenge.txt
	New-Item -ItemType File -Path "C:\Users\Alpha10\Desktop\words.txt" -Force
	function global:Get-8LetterWord() {
	[int32[]]$ArrayofAscii=26,97,26,65,10,48,15,33
	$Complexity=1
	# Complexity:
	# 1 - Pure lowercase ASCII
	# 2 - Mix Uppercase and Lowercase ASCII
	# 3 - ASCII Upper/Lower with Numbers
	# 4 - ASCII Upper/Lower with Numbers and Punctuation
	$WordLength=8
	$NewWord=$NULL
		Foreach ($counter in 1..$WordLength) {
			$pickSet=(Get-Random $complexity)*2
			$NewWord=$NewWord+[char]((Get-Random $ArrayOfAscii[$pickset])+$ArrayOfAscii[$pickset+1])
		}
	Return $NewWord
	}
	
	foreach ($Counter in 1..5254) { 
	Get-8LetterWord >> C:\Users\Alpha10\Desktop\words.txt 
	}
	
Set-ADAccountPassword -Identity Bravo02 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "7566D" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo02\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of times 'gaab' is listed in the file on the desktop." -n > C:\Users\Bravo02\Desktop\challenge.txt
	# no prep necessary

Set-ADAccountPassword -Identity Bravo03 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "1" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo02\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of times 'az' appears in the words.txt file on the desktop." -n > C:\Users\Bravo03\Desktop\challenge.txt
	Write-Output "Hint: 'az', may appear more than one time in a word." -n >> C:\Users\Bravo03\Desktop\challenge.txt
	$AA = [char[]]([char]'a'..[char]'z')
	$BB = [char[]]([char]'a'..[char]'z')
	$CC = [char[]]([char]'a'..[char]'z')
	$DD = [char[]]([char]'a'..[char]'z')
	$(foreach ($A in $AA) {
		"$A"
	}) > C:\Users\Bravo02\Desktop\words.txt
	
	$(foreach ($A in $AA) {
		foreach ($B in $BB) {
			"$A$B"
		}
	}) >> C:\Users\Bravo02\Desktop\words.txt
	
	$(foreach ($A in $AA) {
		foreach ($B in $BB) {
			foreach ($C in $CC) {
				"$A$B$C"
			}
		}
	}) >> C:\Users\Bravo02\Desktop\words.txt
	
	$(foreach ($A in $AA) {
		foreach ($B in $BB) {
			foreach ($C in $CC) {
				foreach ($D in $DD) {
					"$A$B$C$D"
				}
			}
		}
	}) >> C:\Users\Bravo02\Desktop\words.txt

Set-ADAccountPassword -Identity Bravo04 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "2081" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo04\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of words with either 'a' OR 'z', in the word, in the words.txt file on the desktop." -n > C:\Users\Bravo04\Desktop\challenge.txt
	New-Item -ItemType Directory -Path "C:\Users\Bravo02\Desktop" -Force
	Copy-Item C:\Users\Bravo02\Desktop\words.txt C:\Users\Bravo03\Desktop\

Set-ADAccountPassword -Identity Bravo05 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "129054" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo05\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of words meeting the following criteria in the file on the desktop CRITERIA - 'a' appears at least twice, followed by either an a, b, c . .  OR g" -n > C:\Users\Bravo05\Desktop\challenge.txt
	New-Item -ItemType Directory -Path "C:\Users\Bravo02\Desktop" -Force
	Copy-Item C:\Users\Bravo02\Desktop\words.txt C:\Users\Bravo04\Desktop\

Set-ADAccountPassword -Identity Bravo06 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "364" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo06\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the number of unique words in the file on the desktop." -n > C:\Users\Bravo06\Desktop\challenge.txt
	New-Item -ItemType Directory -Path "C:\Users\Bravo02\Desktop" -Force
	Copy-Item C:\Users\Bravo02\Desktop\words.txt C:\Users\Bravo05\Desktop\

Set-ADAccountPassword -Identity Bravo07 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "456976" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo07\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the only line that makes the two files in the Downloads folder different." -n > C:\Users\Bravo07\Desktop\challenge.txt
	New-Item -ItemType Directory -Path "C:\Users\Bravo06\Desktop" -Force
	$AA = [char[]]([char]'a'..[char]'z')
	$BB = [char[]]([char]'A'..[char]'Z')
	$CC = [char[]]([char]'a'..[char]'z')
	$DD = [char[]]([char]'A'..[char]'Z')
	$(foreach ($A in $AA) {
		foreach ($B in $BB) {
			foreach ($C in $CC) {
				foreach ($D in $DD) {
					"$B$A$D$C"
				}
			}
		}
	}) >> C:\Users\Bravo06\Desktop\words.txt
	start-sleep -s 1
	$EE = [char[]]([char]'A'..[char]'Z')
	$FF = [char[]]([char]'m'..[char]'z')
	$GG = [char[]]([char]'A'..[char]'Z')
	$HH = [char[]]([char]'m'..[char]'z')
	$(foreach ($E in $EE) {
		foreach ($F in $FF) {
			foreach ($G in $GG) {
				foreach ($H in $HH) {
					"$E$F$G$H"
				}
			}
		}
	}) >> C:\Users\Bravo06\Desktop\words.txt

Set-ADAccountPassword -Identity Bravo08 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "popeye" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo08\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the name of the built-in cmdlet that performs the wget like function on a Windows system." -n > C:\Users\Bravo08\Desktop\challenge.txt
	Invoke-WebRequest -Uri "https://git.cybbh.space/cbtc-content/students/-/raw/master/cbtc/modules/06_batch_powershell_scripting/assets/bins/new.txt?inline=false" -OutFile "C:\Users\Bravo07\Downloads\new.txt"
	Invoke-WebRequest -Uri "https://git.cybbh.space/cbtc-content/students/-/raw/master/cbtc/modules/06_batch_powershell_scripting/assets/bins/old.txt?inline=false" -OutFile "C:\Users\Bravo07\Downloads\old.txt"

Set-ADAccountPassword -Identity Bravo09 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "invoke-webrequest" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo09\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the last access time of the hosts file.  Note - format for the password is 2 digit month, 2 digit day, 2 digit year. Ex 5 jan 2015 would be 01/05/15." -n > C:\Users\Bravo09\Desktop\challenge.txt
	# no prep necessary

#Set-ADAccountPassword -Identity Bravo10 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$((get-date).AddYears(+3).AddDays(10).ToString("MM/dd/yy"))" -Force)
Set-ADAccountPassword -Identity Bravo10 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "5/9/2099" -Force)
        New-Item -ItemType File -Path "C:\Users\Bravo10\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the 21st line from the top in ASCII-sorted, descending order of the file on the desktop." -n > C:\Users\Bravo10\Desktop\challenge.txt
	Write-Output "Note: Next Level Login - Charlie01" -n >> C:\Users\Bravo10\Desktop\challenge.txt
	Function global:TimeStomp 
	{
	Param (
    [Parameter(mandatory=$true)]
    [string[]]$path,
#    [datetime]$date = (get-date).AddYears(+3).AddDays(10).ToString("MM/dd/yy"))
    [datetime]$date = "05/09/2099")
    Get-ChildItem -Path $path |
    ForEach-Object {
     $_.CreationTime = $date
     $_.LastAccessTime = $date
     $_.LastWriteTime = $date 
	}
	}
	TimeStomp C:\Windows\System32\drivers\etc\hosts

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####  ---  SETTING UP - CHARLIE - USER CHALLENGES  ---    ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
Set-ADAccountPassword -Identity Charlie01 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "ZzZp" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie01\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the date KB3200970 was installed on the server.  Note - format for the password is 2 digit month, 2 digit day, 4 digit year. Ex 5 jan 2015 would be 01/05/2015" -n > C:\Users\Charlie01\Desktop\challenge.txt
	Copy-Item C:\Users\Bravo06\Desktop\words.txt C:\Users\Bravo10\Desktop\
	
#Set-ADUser -Identity "CN=Charlie02,OU=Charlie,OU=CTF,DC=cyber,DC=warriors" -AccountPassword "$((((Get-HotFix –ID KB4091664 | select Installedon) -split ' ')[0] -split '=')[1])"
Set-ADAccountPassword -Identity Charlie02 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "1/2/2019" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie02\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the SID of the current user. Example  S-1-5-21-1004336348-1177238915-[682003330]-1000" -n > C:\Users\Charlie02\Desktop\challenge.txt

Set-ADAccountPassword -Identity Charlie03 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$(((wmic useraccount list brief | select-string 'Charlie02') -split '-')[6])" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie03\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the RID of the 'krbtgt' account. Example  S-1-5-21-1004336348-1177238915-682003330-[501]" -n > C:\Users\Charlie03\Desktop\challenge.txt
	# no prep necessary

Set-ADAccountPassword -Identity Charlie04 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "502" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie04\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the SID of the only Totally-Legit service. Example  S-1-5-80-159957745-2084983471-2137709666-960844832-[1182961511]" -n > C:\Users\Charlie04\Desktop\challenge.txt
	# no prep necessary

Set-ADAccountPassword -Identity Charlie05 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$(((cmd.exe /c 'sc showsid Legit') -split '-')[10])" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie05\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is the name of the program that is set to start at logon." -n > C:\Users\Charlie05\Desktop\challenge.txt
	cmd.exe /c "sc create Legit binpath= C:\windows\system32\kbd101f.cmd start= auto DisplayName= Totally-Legit type= own"

Set-ADAccountPassword -Identity Charlie06 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "yes" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie06\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is in the zip file." -n > C:\Users\Charlie06\Desktop\challenge.txt
	Write-Output "$A = (((wmic useraccount list brief | select-string 'Charlie05') -split "\\")[1] -split " ")[0]" > C:\windows\system32\schd.ps1
	Write-Output "if ( $A -match ('Charlie05') ) { New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\Run" -Name yes -Value "C:\Users\Charlie05\Desktop\no.exe" -PropertyType String | Out-Null}" >> C:\windows\system32\schd.ps1
	$tr = New-JobTrigger -AtLogon -User ctf\Charlie05
	$opts = New-ScheduledJobOption -HideInTaskScheduler -RunElevated -StartIfOnBattery -ContinueIfGoingOnBattery
	Register-ScheduledJob -Name Charlie05 -FilePath  C:\windows\system32\schd.ps1 -ScheduledJobOption $opts -Trigger $tr		

Set-ADAccountPassword -Identity Charlie07 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "kung-fu" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie07\Desktop\challenge.txt" -Force
	Write-Output "The password for the next level is hidden in the users profile." -n > C:\Users\Charlie07\Desktop\challenge.txt
	# creates "password.txt" a smoke-screen
	1..500 | % { Write-Output " i, Charlie06, will not try to take the easy way out again ." -n >> C:\Users\Charlie06\Documents\password.txt }
	Write-Output "" >> C:\Users\Charlie06\Documents\password.txt
	Write-Output " Sincerely," -n >> C:\Users\Charlie06\Documents\password.txt 
	Write-Output " Charlie06" -n >> C:\Users\Charlie06\Documents\password.txt
	Write-Output "" >> C:\Users\Charlie06\Documents\password.txt
	# creates "Charlie1000.zip"
	New-Item -ItemType Directory -Path C:\Users\Charlie06\Documents\archive
	New-Item -ItemType File -Path C:\Users\Charlie06\Documents\Charlie1.txt
	Write-Output "kung-fu" -n > C:\Users\Charlie06\Documents\Charlie1.txt
	Compress-Archive -Path C:\Users\Charlie06\Documents\Charlie1.txt -DestinationPath C:\Users\Charlie06\Documents\Charlie1.zip; Remove-Item C:\Users\Charlie06\Documents\Charlie1.txt
	for ($i=1; $i -lt 1001; $i = $i + 1) { 
	Compress-Archive -Path C:\Users\Charlie06\Documents\Charlie*.zip -DestinationPath C:\Users\Charlie06\Documents\archive\Charlie$i.zip; Remove-Item C:\Users\Charlie06\Documents\Charlie*.zip; Move-Item C:\Users\Charlie06\Documents\archive\Charlie*.zip C:\Users\Charlie06\Documents\; 
	}
	Remove-Item C:\Users\Charlie06\Documents\archive -recurse -force

Set-ADAccountPassword -Identity Charlie08 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "P455W0RD" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie08\Desktop\challenge.txt" -Force
	Write-Output "Challenge Hint - its a crappie site, but someones gotta phish it.." -n > C:\Users\Charlie08\Desktop\challenge.txt
	$FILES = @("nothing_here","empty_file","completely_blank","bit_free")
	foreach ($FILE in $FILES) {
		New-Item -ItemType File -Path "C:\Users\Charlie07\Documents\$FILE" -Force
	}
	Add-Content -Path C:\Users\Charlie07\Documents\nothing_here -Value "P455W0RD" -Stream "hidden"	
	Write-Output "challenges from here on ... get bit more challenging" > C:\Users\Charlie07\Documents\NOTICE

Set-ADAccountPassword -Identity Charlie09 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "phi5hy" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie09\Desktop\challenge.txt" -Force
	Write-Output "Challenge Hint - Beijing to deny any knowledge of injecting cookies onto our systems . ." -n > C:\Users\Charlie09\Desktop\challenge.txt
	Write-Output "" > C:\Windows\Web\crappie
	Write-Output " can not seem to remember where i put that darn lobster trap . . " -n >> C:\Windows\Web\crappie
	Write-Output "" >> C:\Windows\Web\crappie	
	Write-Output " i know it is around here somewhere . ." -n >> C:\Windows\Web\crappie
	Write-Output "" >> C:\Windows\Web\crappie
	New-Item -ItemType Directory "C:\Windows\Web\WWW" -Force
	New-Item -ItemType File "C:\Windows\Web\WWW" -name "getting warmer" -Force
        0..404 | % { attrib -h "C:\Windows\Web\WWW\$_" | Out-Null}
        0..404 | % { Remove-Item "C:\Windows\Web\WWW\$_" | Out-Null}
        0..404 | % { New-Item -ItemType File -Path "C:\Windows\Web\WWW" -Name "$_" -Force}
        0..404 | % { attrib +h "C:\Windows\Web\WWW\$_"}
	attrib -h C:\Windows\Web\WWW\200
	Write-Output "Passsword: phi5hy" > "C:\Windows\Web\WWW\200"
	attrib +h C:\Windows\Web\WWW\200

Set-ADAccountPassword -Identity Charlie10 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "fortune_cookie" -Force)
        New-Item -ItemType File -Path "C:\Users\Charlie10\Desktop\challenge.txt" -Force
	Write-Output "Challenge Hint - let it be logged ..  the password is somewhere on this system . ." -n > C:\Users\Charlie10\Desktop\challenge.txt
	Write-Output "Note: Next Level Login - ??????" -n >> C:\Users\Charlie10\Desktop\challenge.txt
	New-Item -ItemType Directory "C:\Windows\PLA\not_china" -Force

	New-Item -ItemType File "C:\Windows\PLA\not_china\Fortune Cookie Crumb" -Force
	Write-Output "find the hidden fortune cookie.s . . " -n > "C:\Windows\PLA\not_china\Fortune Cookie Crumb"

	Add-Content -Path "C:\windows\PLA\not_china\The Fortune Cookie" -Value "Password:  fortune_cookie" -Stream "none"

	Write-Output "The fortune you seek is inside The Fortune Cookie on this system." -n > "C:\Windows\PLA\not_china\The Fortune Cookie"

        takeown /F "C:\Windows\SysWOW64\Com" /A /R /D Y
        New-Item -ItemType File -Path "C:\Windows\SysWOW64\Com" -Name "fortune cookie.txt" -Force
	Write-Output "out to lunch .. check back in 5 min." -n  > "C:\Windows\SysWOW64\Com\fortune cookie.txt"
	attrib +h "C:\Windows\SysWOW64\Com\fortune cookie.txt"

	Write-Output "I cannot help you, for I am just a cookie." -n  > "C:\Windows\System32\Com\cookie.txt"
	attrib +h "C:\Windows\System32\Com\cookie.txt"

	Write-Output "only listen to The Fortune Cookie, and disregard all other fortune telling units." -n  > "C:\Users\Charlie09\Documents\fortune cookie.txt"
	attrib +h "C:\Users\Charlie09\Documents\fortune cookie.txt"

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####   ---  SETTING UP - DELTA - USER CHALLENGES  ---     ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
Set-ADAccountPassword -Identity Delta01 -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "3v3nt_L0g" -Force)
        New-Item -ItemType File -Path "C:\Users\Delta01\Desktop\challenge.txt" -Force
        Write-Output "Challenge Hint - Arrr! thar be ΘΗΣΑΥΡΟΣ burried in the Share . ." -n > C:\Users\Delta01\Desktop\challenge.txt
	Write-EventLog -LogName "Application" -Source "EventSystem" -EntryType Information -EventId "4625" -category 0 -Message "The description for Event ID '1073746449' in Source EventSystem cannot be found.  The local computer may not have the necessary registry information or message DLL files to display the message, or you may not have permission to access them.  The following information is part of the event:'86400', SuppressDuplicateDuration, Software\Microsoft\EventSystem\EventLog, password: NOT_LIKELY"
	Write-EventLog -LogName "Application" -Source "ESENT" -EntryType Information -EventId "326" -category 1 -Message "Congratulations! NO Password here!"
	Write-EventLog -LogName "System" -Source "Service Control Manager" -EntryType Information -EventId "7036" -category 0 -Message "Congratulations!  you STILL HAVE NOT found the Password"
	Write-EventLog -LogName "Application" -Source "ESENT" -EntryType Information -EventId "326" -category 1 -Message "The DNS Application Directory Partition DomainDnsZones.cyber.warriors was created. The distinguished name of the root of this Directory Partition is DC=DomainDnsZones,DC=cyber,DC=warriors ........................  the Password is: 3v3nt_L0g"

	Write-Output "Arrgh!  fools gold!  Blast!  " -n > "C:\share\CTF\Alpha\2\HOME\1\HOME\8\B00ty"
	attrib +s +h C:\share\CTF\Aplpha\2\HOME\1\HOME\8\B00ty
	Write-Output "Arrgh!  just some old boot!  " -n > "C:\share\CTF\Charlie\8\HOME\3\HOME\7\booty"
	attrib +s +h C:\share\CTF\Charlie\8\HOME\3\HOME\7\booty
	Write-Output "Arrgh!  just some old boot!  " -n > "C:\share\CTF\Bravo\3\HOME\4\HOME\5\BOOTY"
	attrib +s +h C:\share\CTF\Bravo\3\HOME\4\HOME\5\BOOTY
	Write-Output "Arr!  Well Done Matey!  You've Completed the Entire CTF! Final Flag:  b00ty" -n > "C:\share\CTF\Delta\8\HOME\3\HOME\9\ΒΘΘΤΨ"
	attrib +s +h "C:\share\CTF\Delta\8\HOME\3\HOME\9\ΒΘΘΤΨ"
	icacls "C:\share\CTF" /grant Delta01:R /T /C
UnRegister-ScheduledJob -Name Charlie05

New-Item $PROFILE.AllUsersAllHosts -ItemType File -Force
Write-Output '$ProfileRoot = (Split-Path -Parent $MyInvocation.MyCommand.Path)' > C:\Windows\System32\WindowsPowerShell\v1.0\profile.ps1
Write-Output '$env:path += "$ProfileRoot"' >> C:\Windows\System32\WindowsPowerShell\v1.0\profile.ps1

# Apply Read-only home folder/file perms to all regular users:
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####    ---  SET ALL USER FILES/FOLDER READ ONLY  ---     ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
#  If permission mistake made .. remove all user perms:
cmd /c " For /F %a IN ('type C:\Windows\System32\users.txt') DO icacls C:\Users\%a /C /T /reset"
#  Set all user permissions to read-only:
cmd /c " For /F %a IN ('type C:\Windows\System32\users.txt') DO icacls C:\Users\%a /grant %a:R /T /C"
#  Allows users to generate PowerShell ConsoleHost History File on login:
cmd /c " For /F %a IN ('type C:\Windows\System32\users.txt') DO icacls C:\Users\%a\AppData /grant %a:F /T /C"

$NUMS = @("01","02","03","04","05","06","07","08","09","10")
ForEach ($i in $NUMS) {
	New-Item -ItemType File "C:\Users\Alpha$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1" -Force
}
ForEach ($i in $NUMS) {
	New-Item -ItemType File "C:\Users\Bravo$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1" -Force
}
ForEach ($i in $NUMS) {
	New-Item -ItemType File "C:\Users\Charlie$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1" -Force
}
$FEW = @("01","02","03","04")
ForEach ($i in $FEW) {
	New-Item -ItemType File "C:\Users\Delta$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1" -Force
}

# for loop for creating each user $profile
$cuch = @'
 
#ErrorActionPreference = 'SilentlyContinue'
Clear-History
[void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')
[System.Windows.Forms.SendKeys]::SendWait("%{F7}")
Clear-Host

'@

ForEach ($i in $NUMS) {
	 echo $cuch > C:\Users\Alpha$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1
}
ForEach ($i in $NUMS) {
	echo $cuch > C:\Users\Bravo$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1
}
ForEach ($i in $NUMS) {
	echo $cuch > C:\Users\Charlie$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1
}
$FEW = @("01","02","03","04")
ForEach ($i in $FEW) {
	echo $cuch > C:\Users\Delta$i\Documents\WindowsPowerShell\Microsoft.PowerShell_Profile.ps1
}



# Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -name "go" 'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe -noprofile -sta -File "C:\windows\system32\WindowsPowerShell\V1.0\start.ps1"'

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####               ---  CLEAN - UP  ---                   ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
Remove-Item C:\windows\system32\reg.ps1 -Force
Remove-Item C:\windows\system32\schd.ps1 -Force
Remove-Item C:\windows\system32\psexec.exe -force
Remove-Item C:\windows\system32\setup1.ps1 -force
Remove-Item C:\windows\system32\setup2.ps1 -force

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####          ---  CONFIRMING - CLEAN - UP  ---           ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
$reg = (Test-Path "C:\windows\system32\reg.ps1")
$sched = (Test-Path "C:\windows\system32\schd.ps1")
$psex = (Test-Path "C:\windows\system32\psexec.ps1")
$set1 = (Test-Path "C:\windows\system32\setup1.ps1")
$set2 = (Test-Path "C:\windows\system32\setup2.ps1")
if ($reg -eq $false) {
	"WIPED: C:\windows\system32\reg.ps1"
} else {
	'WARNING!  C:\windows\system32\reg.ps1  EXISTS!'
}
if ($sched -eq $false) {
	"WIPED: C:\windows\system32\sched.ps1"
} else {
	'WARNING!  C:\windows\system32\sched.ps1  EXISTS!'
}
if ($psex -eq $false) {
	"WIPED: C:\windows\system32\psexec.exe"
} else {
	'WARNING!  C:\windows\system32\psexec.exe  EXISTS!'
}
if ($set1 -eq $false) {
	"WIPED: C:\windows\system32\setup1.ps1"
} else {
	'WARNING!  C:\windows\system32\setup1.ps1  EXISTS!'
}
if ($set2 -eq $false) {
	"WIPED: C:\windows\system32\setup2.ps1"
} else {
	'WARNING!  C:\windows\system32\setup2.ps1  EXISTS!'
}

Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####               ---  KILL HISTORY ---                  ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
Clear-History
#  Sends [ALT] + F7 to clear buffer as well:
[void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')
[System.Windows.Forms.SendKeys]::SendWait("%{F7}") 

Start-Sleep -s 5
Write-Host ""
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host " ####                                                      ####"
Write-Host " ####    ---  F I N I S H E D  -  R E B O O T I N G ---    ####"
Write-Host " ####                                                      ####"
Write-Host " ##############################################################"
Write-Host " ##############################################################"
Write-Host ""
Restart-Computer
