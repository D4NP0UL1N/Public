[System.Net.ServicePointManager]::SecurityProtocol = @("TLS12","TLS11","TLS","SSL3")
Invoke-WebRequest -Uri "https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.1.0.0p1-Beta/OpenSSH-Win64.zip" -OutFile "C:\OpenSSH-Win64.zip"
Unblock-File C:\OpenSSH-Win64.zip
Expand-Archive C:\OpenSSH-Win64.zip -DestinationPath C:\
icacls  C:\OpenSSH-Win64\libcrypto.dll /grant Everyone:RX
C:\OpenSSH-Win64\install-sshd.ps1
sc.exe config sshd start= auto
sc.exe config ssh-agent start= auto
sc.exe start sshd
sc.exe start ssh-agent
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH SSH Server' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22 -Program "C:\Windows\System32\sshd.exe"
cp "C:\OpenSSH-Win64\sshd.exe" C:\windows\system32\
cp "C:\OpenSSH-Win64\ssh.exe" C:\windows\system32\
