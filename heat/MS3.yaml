heat_template_version: 2018-08-31

description: Heat Template for BCC Pilot - MS3

parameters:

  student_id:
    type: string
    label: Student ID
    description: Student ID Number with No Leading Zeros

  last_name:
    type: string
    label: Last Name
    description: Last Name
    default:
    constraints:
      -  length: { min: 1, max: 15 }
         description: Last name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: Last name may only contain letters

  password:
    type: string
    label: Password
    description: Set root/admin password for instances
    hidden: true
    default:
    constraints:
      -  length: { min: 8, max: 20 }
         description: Password must be between 8 and 20 characters
      -  allowed_pattern: "[a-zA-Z0-9]*"
         description: Password may not contain special characters

resources:

  stu-network:
    type: OS::Neutron::Net
    properties:
      name:
        str_replace:
          template: lastName_Network
          params:
            lastName: { get_param: last_name }
      admin_state_up: true
      shared: false

  stu-subnet:
    type: OS::Neutron::Subnet
    properties:
      allocation_pools:
        - start:
            str_replace:
              template: 10.studentID.0.200
              params:
                studentID: { get_param: student_id }
          end:
            str_replace:
              template: 10.studentID.0.250
              params:
                studentID: { get_param: student_id }
      cidr:
        str_replace:
          template: 10.studentID.0.0/24
          params:
            studentID: { get_param: student_id }
      gateway_ip:
        str_replace:
          template: 10.studentID.0.254
          params:
            studentID: { get_param: student_id }
      network_id: { get_resource: stu-network }
      dns_nameservers: [10.50.255.254]
      enable_dhcp: true
      host_routes: []
      ip_version: 4
      name:
        str_replace:
          template: lastname_subnet
          params:
            lastname: { get_param: last_name }

  stu-router:
    type: OS::Neutron::Router    
    properties:
      name:
        str_replace:
          template: lastname_router
          params:
            lastname: { get_param: last_name }
      external_gateway_info: {"network": public}

  stu-router-interface:
    type:  OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: stu-router }       
      subnet_id: { get_resource: stu-subnet }
      
  float_ip_1:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_1:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.5
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
      
  float_ip_assoc_1:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_1 }
      port_id: { get_resource: float_port_1 }

  host1:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: MS3
          params:
            lastname: { get_param: last_name }
      image: Ubuntu 14.04
      flavor: cy.medium
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_1 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash  
            # MS3
            export DEBIAN_FRONTEND=noninteractive
            echo 127.0.0.1 $(hostname) >> /etc/hosts
            apt-get -yq update
            sleep 30
            apt-get install -yq locate dnsutils lsof aptitude ftp auditd xinetd telnetd samba git zip unzip hexedit libncurses5-dev ncurses-hexedit gcc
            apt-get install -yq build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf tree
            apt-get install -yq pgadmin3 curl ethtool zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev xrdp netcat firefox lolcat apache2
            apt-get -yq install figlet
            apt-get -yq upgrade
            gem install lolcat
            updatedb
            mandb
             
            # ----- ENABLES TELNET
            cat > /etc/xinetd.d/telnet <<"__TELNET__"
            # default: on
            # description: The telnet server serves telnet sessions; it uses
            # unencrypted username/password pairs for authentication.
            service telnet
            {
            disable = no
            flags = REUSE
            socket_type = stream
            wait = no
            user = root
            server = /usr/sbin/in.telnetd
            log_on_failure += USERID
            }
            __TELNET__
            for x in {0..9}; do echo "pts/$x" >> /etc/securetty ; done
            sleep 30
            # systemctl restart xinetd.service
            
            # ----- HOSTNAME setting ubuntu/debian:
            hostnamectl set-hostname 'MS3'
            hostname MS3
            
            useradd $user -m -U -G sudo -s /bin/bash
            useradd -m cadre -U -G sudo -s /bin/bash
            echo "$user:$password" | chpasswd
            echo "cadre:PassWord1234" | chpasswd
            
            # ----- MOTD:
            cat > /etc/motd <<"__MOTD__"
              ******************************************************************
             *                                                                   *
             *  This system is for the use of authorized users only.  Usage of   *
             *  this system may be monitored and recorded by system personnel.   *
             *                                                                   *
             *  Anyone using this system expressly consents to such monitoring   *
             *  and is advised that if such monitoring reveals possible          *
             *  evidence of criminal activity, system personnel may provide the  *
             *  evidence from such monitoring to law enforcement officials.      *
             *                                                                   *
              ******************************************************************
            __MOTD__
            
            # --- install metasploitable3
            echo "Setup Metasploitable3"
            apt-get install -y curl git
            cd / && git clone https://github.com/rapid7/metasploitable3.git
            curl -L https://omnitruck.chef.io/install.sh | bash -s -- -v 13.8.5
            # curl -L https://omnitruck.chef.io/install.sh | bash -s -- -v 14.4.56
            mkdir /var/chef
            
            cat > "/metasploitable3/chef/cookbooks/ms3.json" << __EOF__
            {
              "run_list": [
                "metasploitable::users",
                "metasploitable::mysql",
                "metasploitable::apache_continuum",
                "metasploitable::apache",
                "metasploitable::php_545",
                "metasploitable::phpmyadmin",
                "metasploitable::proftpd",
                "metasploitable::docker",
                "metasploitable::samba",
                "metasploitable::sinatra",
                "metasploitable::unrealircd",
                "metasploitable::chatbot",
                "metasploitable::payroll_app",
                "metasploitable::readme_app",
                "metasploitable::cups",
                "metasploitable::drupal",
                "metasploitable::knockd",
                "metasploitable::iptables",
                "metasploitable::flags"
              ]
            }
            
            __EOF__
            
            chef-solo -j /metasploitable3/chef/cookbooks/ms3.json --config-option cookbook_path=/metasploitable3/chef/cookbooks
            
            # --- setup SSH
            echo "SETUP SSH"
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
            sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
            # systemctl restart ssh
            
            useradd -M -N -G sudo -s /bin/bash instructor
            echo "cadre:password" | chpasswd
            echo "root:password" | chpasswd
            
            # -- setup TCP Offload
            echo "SETUP TCP Offload"
            # /sbin/ethtool -K eth0 tx off sg off tso off
            
            # ----- UBUNTU Spice breaks graphical.target
            # systemctl set-default multi-user.target
            reboot
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
      user_data_format: RAW