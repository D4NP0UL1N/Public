heat_template_version: 2018-08-31

description: Heat Template for CTFd Server Build - Ubuntu 18.04

parameters:

  student_id:
    type: string
    label: Student ID
    description: Student ID Number with No Leading Zeros
    default: 0

  last_name:
    type: string
    label: Last Name
    description: Last Name
    default:
    constraints:
      -  length: { min: 1, max: 15 }
         description: Last name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: Last name may only contain letters

  password:
    type: string
    label: Password
    description: Set root/admin password for instances
    hidden: true
    default: 
    constraints:
      -  length: { min: 8, max: 20 }
         description: Password must be between 8 and 20 characters
      -  allowed_pattern: "[a-zA-Z0-9]*"
         description: Password may not contain special characters
        
  token:
    type: string
    label: GitLab API Secure Token
    description: Instructor-Generated API Secure Token for Internal Access
    hidden: false

resources:

  stu-network:
    type: OS::Neutron::Net
    properties:
      name:
        str_replace:
          template: lastName_Network
          params:
            lastName: { get_param: last_name }
      admin_state_up: true
      shared: false

  stu-subnet:
    type: OS::Neutron::Subnet
    properties:
      allocation_pools:
        - start:
            str_replace:
              template: 10.studentID.0.200
              params:
                studentID: { get_param: student_id }
          end:
            str_replace:
              template: 10.studentID.0.250
              params:
                studentID: { get_param: student_id }
      cidr:
        str_replace:
          template: 10.studentID.0.0/24
          params:
            studentID: { get_param: student_id }
      gateway_ip:
        str_replace:
          template: 10.studentID.0.254
          params:
            studentID: { get_param: student_id }
      network_id: { get_resource: stu-network }
      dns_nameservers: [10.50.255.254]
      enable_dhcp: true
      host_routes: []
      ip_version: 4
      name:
        str_replace:
          template: lastname_subnet
          params:
            lastname: { get_param: last_name }

  stu-router:
    type: OS::Neutron::Router    
    properties:
      name:
        str_replace:
          template: lastname_router
          params:
            lastname: { get_param: last_name }
      external_gateway_info: { "network": public }

  stu-router-interface:
    type:  OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: stu-router }       
      subnet_id: { get_resource: stu-subnet }

  float_ip_1:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_1:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.1
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false

  float_ip_assoc_1:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_1 }
      port_id: { get_resource: float_port_1 }

  host1:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: CTFd Server
          params:
            lastname: { get_param: last_name }
      image: Ubuntu 18.04
      flavor: cy.mega
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_1 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            export DEBIAN_FRONTEND=noninteractive
            echo 127.0.0.1 $(hostname) >> /etc/hosts
            apt-get update -y
            apt-get install -y locate dnsutils binutils netcat lsof gunicorn aptitude auditd git zip unzip rename figlet hexedit apache2 gcc build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf pgadmin3 curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev firefox tree
            sleep 0.5
            apt-get -y upgrade
            updatedb
            mandb
            # ----- ENABLES SSH FOR INSTRUCTORS
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
            systemctl restart ssh.service
            # --- Install docker
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
            add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
            apt-get update -y && apt-get upgrade -y && apt-get install -y git docker-ce
            # --- Sets mtu to 1450 on all but loopback interfaces:
            ifconfig | egrep -v "lo:" | grep mtu | awk ' { print $1 } ' | sed 's/://g' | while read x; do ifconfig $x mtu 1450; done
            # --- Install docker-compose
            curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose
            # ----- CTFd install
            cd /opt/ && git clone https://github.com/CTFd/CTFd
            python -c "import os; print repr(os.urandom(64))" > /opt/CTFd/.ctfd_secret_key
            printf '{\n  "mtu": 1450,\n  "dns": ["10.50.255.254"]\n}' > /etc/docker/daemon.json
            systemctl restart docker.service
            # --- Config CTFd to Cyber theme
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fthemes%2flearning_theme_20181202.zip/raw?ref=master' -o /opt/CTFd/CTFd/themes/learning_theme.zip
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fthemes%2fhacker-theme_20181202.zip/raw?ref=master' -o /opt/CTFd/CTFd/themes/hacker_theme.zip
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fplugins%2fmultiple_choice_20190115.zip/raw?ref=master' -o /opt/CTFd/CTFd/plugins/multiple_choice.zip
            cd /opt/CTFd/CTFd/plugins && unzip /opt/CTFd/CTFd/plugins/multiple_choice.zip
            cd /opt/CTFd/CTFd/themes && unzip /opt/CTFd/CTFd/themes/learning_theme.zip && unzip /opt/CTFd/CTFd/themes/hacker_theme.zip
            cd /opt/CTFd/CTFd/themes && zip -r core.orig.zip ./core && mv /opt/CTFd/CTFd/themes/learning /opt/CTFd/CTFd/themes/cctcv2
            if ! $(find /opt/CTFd/CTFd/themes -maxdepth 1 -type d -name 'learning'); then echo "DIR LEARNING NOT MOVED TO CCTCv2"; else echo "DIR LEARNING MOVED TO CCTCv2"; fi
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fimages%2fcyber-official.png/raw?ref=master' -o /opt/CTFd/CTFd/themes/cctcv2/static/img/cyber-official.png
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fimages%2fcyber-school-large.gif/raw?ref=master' -o /opt/CTFd/CTFd/themes/cctcv2/static/img/cyber-school-large.gif
            sed -i 's/favicon.ico/cyber-official.png/g' /opt/CTFd/CTFd/themes/cctcv2/templates/base.html
            mv /opt/CTFd/CTFd/themes/cctcv2/static/css/base.css /opt/CTFd/CTFd/themes/cctcv2/static/css/base.css.bak
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fpages%2fbase.css/raw?ref=master' -o /opt/CTFd/CTFd/themes/cctcv2/static/css/base.css
            mv /opt/CTFd/CTFd/themes/cctcv2/static/css/challenge-board.css /opt/CTFd/CTFd/themes/cctcv2/static/css/challenge-board.css.bak
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fpages%2fchallenge-board.css/raw?ref=master' -o /opt/CTFd/CTFd/themes/cctcv2/static/css/challenge-board.css
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fpages%2findex.html/raw?ref=master' -o /opt/CTFd/CTFd/index.html
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fpages%2ffaq.html/raw?ref=master' -o /opt/CTFd/CTFd/themes/cctcv2/templates/faq.html
            rm -f /opt/CTFd/CTFd/themes/cctcv2/templates/setup.html
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fpages%2fsetup.html/raw?ref=master' -o /opt/CTFd/CTFd/themes/cctcv2/templates/setup.html
            cp /opt/CTFd/CTFd/views.py /opt/CTFd/CTFd/views.py.orig
            sed -i '/# Index page/,/# Visibility/{//!d}' /opt/CTFd/CTFd/views.py
            sed -i '/# Index page/r /opt/CTFd/CTFd/index.html' /opt/CTFd/CTFd/views.py
            # ----- INSTALLS LOGGING RULES INTO /etc/rsyslog.conf <not default in UBUNTU>
            cat > /tmp/log_rules.txt <<"__LOGS1__"
            ###############
            #### RULES ####
            ###############
            #
            # First some standard log files.  Log by facility.
            #
            auth,authpriv.*			/var/log/auth.log
            *.*;auth,authpriv.none	-/var/log/syslog
            #cron.*				    /var/log/cron.log
            daemon.*			    -/var/log/daemon.log
            kern.*				    -/var/log/kern.log
            lpr.*				    -/var/log/lpr.log
            mail.*				    -/var/log/mail.log
            user.*				    -/var/log/user.log
            __LOGS1__
            sed '/$IncludeConfig \/etc\/rsyslog.d\/\*.conf/r /tmp/log_rules.txt' -i /etc/rsyslog.conf
            systemctl restart rsyslog.service
            mkdir -p /var/log/journal
            # --- Pull CTFd deploy script and run (reboot initialized at the conclusion of script):
            curl --request GET --header "PRIVATE-TOKEN: $api"  'https://git.cybbh.space/api/v4/projects/237/repository/files/ctfd%2fconfigs%2fscripts%2fctfd_deploy.sh/raw?ref=master' -o /opt/CTFd/CTFd/ctfd_deploy.sh
            chmod +x /opt/CTFd/CTFd/ctfd_deploy.sh
            if [[ -x /opt/CTFd/CTFd/ctfd_deploy.sh ]]; then echo "SCRIPT EXISTS AND IS EXECUTABLE" && (bash -c /opt/CTFd/CTFd/ctfd_deploy.sh) & fi
            useradd $user -m -U -G sudo -s /bin/bash
            echo "root:$password" | chpasswd
            echo "$user:$password" | chpasswd
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token }
      user_data_format: RAW