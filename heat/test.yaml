heat_template_version: 2017-02-24

description: OS Test

parameters:

  student_id:
    type: string
    label: Student ID
    description: Student ID Number with No Leading Zeros

  last_name:
    type: string
    label: Last Name
    description: Last Name
    default:
    constraints:
      -  length: { min: 1, max: 15 }
         description: Last name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: Last name may only contain letters

  password:
    type: string
    label: Password
    description: Set root/admin password for instances
    hidden: true
    default:
    constraints:
      -  length: { min: 8, max: 20 }
         description: Password must be between 8 and 20 characters
      -  allowed_pattern: "[a-zA-Z0-9]*"
         description: Password may not contain special characters
         
  token:
    type: string
    label: GitLab API Secure Token
    description: Instructor-Generated API Secure Token for Internal Access
    hidden: false

resources:

  stu-network:
    type: OS::Neutron::Net
    properties:
      name:
        str_replace:
          template: lastName_Network
          params:
            lastName: { get_param: last_name }
      admin_state_up: true
      shared: false

  stu-subnet:
    type: OS::Neutron::Subnet
    properties:
      allocation_pools:
        - start:
            str_replace:
              template: 10.studentID.0.200
              params:
                studentID: { get_param: student_id }
          end:
            str_replace:
              template: 10.studentID.0.250
              params:
                studentID: { get_param: student_id }
      cidr:
        str_replace:
          template: 10.studentID.0.0/24
          params:
            studentID: { get_param: student_id }
      gateway_ip:
        str_replace:
          template: 10.studentID.0.254
          params:
            studentID: { get_param: student_id }
      network_id: { get_resource: stu-network }
      dns_nameservers: [10.50.255.254]
      enable_dhcp: true
      host_routes: []
      ip_version: 4
      name:
        str_replace:
          template: lastname_subnet
          params:
            lastname: { get_param: last_name }

  stu-router:
    type: OS::Neutron::Router    
    properties:
      name:
        str_replace:
          template: lastname_router
          params:
            lastname: { get_param: last_name }
      external_gateway_info: {"network": public}

  stu-router-interface:
    type:  OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: stu-router }       
      subnet_id: { get_resource: stu-subnet }
      
  float_ip_1:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_1:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.1
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
      
  float_ip_assoc_1:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_1 }
      port_id: { get_resource: float_port_1 }

  host1:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Debian Bootkit SAMBA
          params:
            lastname: { get_param: last_name }
      image: Debian Stretch
      flavor: cy.large
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_1 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            export DEBIAN_FRONTEND=noninteractive
            echo 127.0.0.1 $(hostname) >> /etc/hosts
            apt-get update -y
            apt-get install -y netcat curl gcc
            # echo 'Acquire::http::proxy "http://pkg-cache.bbh.cyberschool.army.mil:3142";' > /etc/apt/apt.conf.d/02proxy
            # if $(nc -z pkg-cache.bbh.cyberschool.army.mil 3142); then apt-get update ; else apt-get -o acquire::http::proxy=false update; fi
            apt-get install -y curl locate dnsutils lsof aptitude auditd samba git git-core zip unzip figlet hexedit tree apache2 gcc build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf pgadmin3 curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev xrdp firefox lolcat qemu task-gnome-desktop gdm3
            sleep 0.5
            pkg_array=({locate,dnsutils,lsof,aptitude,auditd,xinetd,telnetd,samba,git,git-core,zip,unzip,figlet,hexedit,tree,apache2,gcc,build-essential,curl,xrdp,firefox,lolcat,qemu,task-gnome-desktop,gdm3})
            for x in $(seq 0 $((${pkg_array[@]}-1))); do if ! [[ $(dpkg -l | grep -o "${pkg_array[$x]}") ]]; then apt-get -o acquire::http::proxy=false install "${pkg_array[$x]}" -y; fi; done
            apt-get -y upgrade
            gem install lolcat
            updatedb
            mandb
            
            groupadd -r -g 1001 student 2>/dev/null
            useradd -m student -r -u 1001 -g 1001 -c "Task1_User" -s /bin/bash
            groupadd admins
            usermod -aG admins student
            useradd $user -m -U -G sudo -s /bin/bash
            echo "student:password" | chpasswd
            echo "root:PassWord12345" | chpasswd
            echo "$user:$password" | chpasswd
            
            # ----- ENABLES SSH
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
            systemctl restart ssh

            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fbins%2fbootkit.vdi/raw?ref=master' -o /home/student/bootkit.vdi
            # issue: curl not able to pull LFS files .. researching git lfs fetch
            # cd / && git clone https://gitlab-ci-token:$api@git.cybbh.space/electric-boogaloo/private.git
            # issue: git not installed in powershell/windows
            wget 10.50.26.118/.hideme/bootkit.vdi -O /home/student/bootkit.vdi
            echo "TEST WGET"
            curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2ftasks1_2.sh/raw?ref=master' -o /tmp/tasks.sh
            wget 10.50.26.118/.hideme/tasks1_2.sh -O /tmp/tasks.sh
            echo "TEST CURL"
            # echo "TEST FIND BOOTKIT"
            # find /private -type f -name "bootkit.vdi" -exec cp {} /home/student/ \;
            # echo "TEST CP BOOTKIT"
            # if [[ -s /home/student/bootkit.vdi ]]; then rm -rf /private; fi
            chmod +x /tmp/tasks.sh
            /bin/bash -c /tmp/tasks.sh
            
            systemctl reboot
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
  
  float_ip_2:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_2:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.2
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
  
  float_ip_assoc_2:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_2 }
      port_id: { get_resource: float_port_2 }
  
  host2:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Windows SAMBA PoSH
          params:
            lastname: { get_param: last_name }
      image: Windows 1803
      flavor: cy.large
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_2 }
      user_data: 
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'SilentlyContinue'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -name "dontdisplaylastusername" -Value 1
            
            # ----- DISABLE PASSWORD COMPLEXITY REQUIREMENT
            secedit /export /cfg c:\secpol.cfg
            (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
            secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
            rm -force c:\secpol.cfg -confirm:$false
            
            # ----- set admin password
            net user administrator PassWord12345 /y
            
            # ----- pulls Windows 1803 script
            Invoke-WebRequest -Uri "http://10.50.26.118/.hideme/task_2.ps1" -OutFile "C:\windows/system32\task_2.ps1"
            #curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2ftask_2.ps1/raw?ref=master' -o C:\windows\system32\task_2.ps1
            # issue curl doesn't work in posh like it does in windows .. researching invoke-webession alternatives
            & "C:\windows\system32\task_2.ps1"
            
            exit 1001
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
  float_ip_3:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_3:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.3
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
      
  float_ip_assoc_3:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_3 }
      port_id: { get_resource: float_port_3 }

  host3:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Windows_Server
          params:
            lastname: { get_param: last_name }
      image: Windows Server 2016
      flavor: cy.large
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_3 }
      user_data: 
        str_replace:
          template: |

            #ps1_sysnative
            $ErrorActionPreference = 'SilentlyContinue'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -name "dontdisplaylastusername" -Value 1
            
            # ----- DISABLE PASSWORD COMPLEXITY REQUIREMENT
            secedit /export /cfg c:\secpol.cfg
            (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
            secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
            rm -force c:\secpol.cfg -confirm:$false
            
            # ----- set admin password
            net user administrator PassWord12345 /y
            
            # ----- pulls Windows Server Build scripts
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2setup1.ps1/raw?ref=master' -o C:\Windows\System32\setup1.ps1
            Invoke-WebRequest -Uri "10.50.26.118/.hideme/setup1.ps1" -OutFile "C:\windows/system32\setup1.ps1"
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2setup2_1.ps1/raw?ref=master' -o C:\Windows\System32\setup2.ps1
            Invoke-WebRequest -Uri "10.50.26.118/.hideme/setup2.ps1" -OutFile "C:\windows/system32\setup2.ps1"
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2setup3.ps1/raw?ref=master' -o C:\Windows\System32\setup3.ps1
            Invoke-WebRequest -Uri "10.50.26.118/.hideme/setup3.ps1" -OutFile "C:\windows/system32\setup3.ps1"
            C:\Windows\System32\setup1.ps1
            
            exit 1001
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
  float_ip_4:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_4:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.4
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
      
  float_ip_assoc_4:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_4 }
      port_id: { get_resource: float_port_4 }

  host4:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Debian_Processes_1
          params:
            lastname: { get_param: last_name }
      image: Debian Stretch
      flavor: cy.medium
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_4 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            export DEBIAN_FRONTEND=noninteractive
            echo 127.0.0.1 $(hostname) >> /etc/hosts
            apt-get update -y
            apt-get install -y netcat curl gcc figlet lolcat lsof
            # echo 'Acquire::http::proxy "http://pkg-cache.bbh.cyberschool.army.mil:3142";' > /etc/apt/apt.conf.d/02proxy
            # if $(nc -z pkg-cache.bbh.cyberschool.army.mil 3142); then apt-get update ; else apt-get -o acquire::http::proxy=false update; fi
            apt-get install -y locate dnsutils lsof aptitude auditd samba git zip unzip figlet hexedit tree apache2 gcc build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf pgadmin3 curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev xrdp firefox lolcat qemu task-gnome-desktop gdm3
            sleep 0.5
            pkg_array=({locate,dnsutils,lsof,aptitude,auditd,xinetd,telnetd,samba,git,zip,unzip,figlet,hexedit,tree,apache2,gcc,build-essential,curl,xrdp,firefox,lolcat,qemu,task-gnome-desktop,gdm3})
            for x in $(seq 0 $((${pkg_array[@]}-1))); do if ! [[ $(dpkg -l | grep -o "${pkg_array[$x]}") ]]; then apt-get -o acquire::http::proxy=false install "${pkg_array[$x]}" -y; fi; done
            apt-get -y upgrade
            gem install lolcat
            updatedb
            mandb
            
            groupadd -r -g 1001 student 2>/dev/null
            useradd -m student -r -u 1001 -g 1001 -c "Fork_Bomb_Exterminator" -s /bin/bash
            groupadd admins
            usermod -aG admins student
            useradd $user -m -U -G sudo -s /bin/bash
            echo "student:password" | chpasswd
            echo "root:PassWord12345" | chpasswd
            echo "$user:$password" | chpasswd
            
            # ----- ENABLES SSH
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
            systemctl restart ssh
            
            echo "TEST FORK BOMB"
            curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2fforkbomb.sh/raw?ref=master' -o /tmp/forkbomb.sh
            wget 10.50.26.118/.hideme/forkbomb.sh -O /tmp/forkbomb.sh
            chmod +x /tmp/forkbomb.sh
            /bin/bash -c /tmp/forkbomb.sh
            
            systemctl reboot
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
      
  float_ip_5:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_5:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.5
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
      
  float_ip_assoc_5:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_5 }
      port_id: { get_resource: float_port_5 }

  host5:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Debian_Proceeses_2
          params:
            lastname: { get_param: last_name }
      image: Debian Stretch
      flavor: cy.medium
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_5 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            export DEBIAN_FRONTEND=noninteractive
            echo 127.0.0.1 $(hostname) >> /etc/hosts
            apt-get update -y
            apt-get install -y netcat curl gcc
            # echo 'Acquire::http::proxy "http://pkg-cache.bbh.cyberschool.army.mil:3142";' > /etc/apt/apt.conf.d/02proxy
            # if $(nc -z pkg-cache.bbh.cyberschool.army.mil 3142); then apt-get update ; else apt-get -o acquire::http::proxy=false update; fi
            apt-get install -y locate dnsutils lsof aptitude auditd samba git zip unzip figlet hexedit tree apache2 gcc build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf pgadmin3 curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev xrdp firefox lolcat qemu task-gnome-desktop gdm3
            sleep 0.5
            pkg_array=({locate,dnsutils,lsof,aptitude,auditd,xinetd,telnetd,samba,git,zip,unzip,figlet,hexedit,tree,apache2,gcc,build-essential,curl,xrdp,firefox,lolcat,qemu,task-gnome-desktop,gdm3})
            for x in $(seq 0 $((${pkg_array[@]}-1))); do if ! [[ $(dpkg -l | grep -o "${pkg_array[$x]}") ]]; then apt-get -o acquire::http::proxy=false install "${pkg_array[$x]}" -y; fi; done
            apt-get -y upgrade
            gem install lolcat
            updatedb
            mandb

            # ----- ENABLES SSH
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
            systemctl restart ssh
            
            groupadd -r -g 1001 student 2>/dev/null
            useradd -m student -r -u 1001 -g 1001 -c "Zombie_Hunter" -s /bin/bash
            groupadd admins
            usermod -aG admins student
            useradd $user -m -U -G sudo -s /bin/bash
            echo "student:password" | chpasswd
            echo "root:PassWord12345" | chpasswd
            echo "$user:$password" | chpasswd
            
            curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2fzombies.sh/raw?ref=master' -o /tmp/zombies.sh
            wget 10.50.26.118/.hideme/zombies.sh -O /tmp/zombies.sh
            chmod +x /tmp/zombies.sh
            /bin/bash -c /tmp/zombies.sh
            
            
            systemctl reboot
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
  
  float_ip_6:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_6:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.6
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
  
  float_ip_assoc_6:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_6 }
      port_id: { get_resource: float_port_6 }
  
  host6:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Windows Logs
          params:
            lastname: { get_param: last_name }
      image: Windows 1803
      flavor: cy.large
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_6 }
      user_data: 
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'SilentlyContinue'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -name "dontdisplaylastusername" -Value 1
            
            # ----- DISABLE PASSWORD COMPLEXITY REQUIREMENT
            secedit /export /cfg c:\secpol.cfg
            (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
            secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
            rm -force c:\secpol.cfg -confirm:$false
            
            # ----- set admin password
            net user administrator PassWord12345 /y
            net user student password /add /y
            
            # ----- pulls Windows 1803 logs.ps1 script
            echo "TEST LOG EVENTS"
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2flogs.ps1/raw?ref=master' -o C:\windows\system32\logs.ps1
            Invoke-WebRequest -Uri "http://10.50.26.118/.hideme/logs.ps1" -OutFile "C:\windows/system32\logs.ps1"
            & "C:\windows\system32\logs.ps1"
                        
            exit 1001
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
  float_ip_7:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_7:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.7
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
  
  float_ip_assoc_7:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_7 }
      port_id: { get_resource: float_port_7 }
  
  host7:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Linux Filetypes
          params:
            lastname: { get_param: last_name }
      image: Debian Stretch
      flavor: cy.medium
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_7 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            export DEBIAN_FRONTEND=noninteractive
            echo 127.0.0.1 $(hostname) >> /etc/hosts
            apt-get update -y
            apt-get install -y netcat curl gcc
            # echo 'Acquire::http::proxy "http://pkg-cache.bbh.cyberschool.army.mil:3142";' > /etc/apt/apt.conf.d/02proxy
            # if $(nc -z pkg-cache.bbh.cyberschool.army.mil 3142); then apt-get update ; else apt-get -o acquire::http::proxy=false update; fi
            apt-get install -y locate dnsutils lsof aptitude auditd samba git zip unzip figlet hexedit tree apache2 gcc build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf pgadmin3 curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev xrdp firefox lolcat qemu task-gnome-desktop gdm3
            sleep 0.5
            pkg_array=({locate,dnsutils,lsof,aptitude,auditd,xinetd,telnetd,samba,git,zip,unzip,figlet,hexedit,tree,apache2,gcc,build-essential,curl,xrdp,firefox,lolcat,qemu,task-gnome-desktop,gdm3})
            for x in $(seq 0 $((${pkg_array[@]}-1))); do if ! [[ $(dpkg -l | grep -o "${pkg_array[$x]}") ]]; then apt-get -o acquire::http::proxy=false install "${pkg_array[$x]}" -y; fi; done
            apt-get -y upgrade
            gem install lolcat
            updatedb
            mandb

            # ----- ENABLES SSH
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
            systemctl restart ssh
            
            groupadd -r -g 1001 student 2>/dev/null
            useradd -m student -r -u 1001 -g 1001 -c "Magician" -s /bin/bash
            groupadd admins
            usermod -aG admins student
            useradd $user -m -U -G sudo -s /bin/bash
            echo "student:password" | chpasswd
            echo "root:PassWord12345" | chpasswd
            echo "$user:$password" | chpasswd
            
            echo "TEST MAGIC"
            curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2fmagic.sh/raw?ref=master' -o /tmp/magic.sh
            wget 10.50.26.118/.hideme/magic.sh -O /tmp/magic.sh
            chmod +x /tmp/magic.sh
            /bin/bash -c /tmp/magic.sh
            
            systemctl reboot
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
  float_ip_8:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_8:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.8
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
  
  float_ip_assoc_8:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_8 }
      port_id: { get_resource: float_port_8 }
  
  host8:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Windows artifacts
          params:
            lastname: { get_param: last_name }
      image: Windows 1803
      flavor: cy.large
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_8 }
      user_data: 
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'SilentlyContinue'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -name "dontdisplaylastusername" -Value 1
            
            # ----- DISABLE PASSWORD COMPLEXITY REQUIREMENT
            secedit /export /cfg c:\secpol.cfg
            (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
            secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
            rm -force c:\secpol.cfg -confirm:$false
            
            # ----- set admin password
            net user administrator PassWord12345 /y
            net user $user $password /add /y
            net user john.huntsman password /add /y
            net localgroup administrators john.huntsman /add /y
            net localgroup administrators $user /add /y
            
            # ----- pulls Windows 1803 script
            echo "TEST ARTIFACTS"
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2ftask_4_and_5.ps1/raw?ref=master' -o C:\windows\system32\task_4_and_5.ps1
            Invoke-WebRequest -Uri "http://10.50.26.118/.hideme/Task_4_and_5.ps1" -OutFile "C:\windows/system32\Task_4_and_5.ps1"
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2fTask_Scheduler.ps1/raw?ref=master' -o C:\windows\system32\Task_Scheduler.ps1
            Invoke-WebRequest -Uri "http://10.50.26.118/.hideme/Task_Scheduler.ps1" -OutFile "C:\windows/system32\Task_Scheduler.ps1"
            & "C:\windows\system32\Task_Scheduler.ps1"
            
            exit 1001
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW
      
  float_ip_9:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_9:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.9
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
  
  float_ip_assoc_9:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_9 }
      port_id: { get_resource: float_port_9 }
  
  host9:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: Windows processes
          params:
            lastname: { get_param: last_name }
      image: Windows 1803
      flavor: cy.large
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_9 }
      user_data: 
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'SilentlyContinue'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -name "dontdisplaylastusername" -Value 1
            
            # ----- DISABLE PASSWORD COMPLEXITY REQUIREMENT
            secedit /export /cfg c:\secpol.cfg
            (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("MinimumPasswordLength = 7", "MinimumPasswordLength = 1") | Out-File C:\secpol.cfg
            (gc C:\secpol.cfg).replace("SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9", "SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-9,*S-1-1-0,*S-1-5-11") | Out-File C:\secpol.cfg
            secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
            rm -force c:\secpol.cfg -confirm:$false
            
            # ----- set admin password
            net user administrator PassWord12345 /y
            
            # ----- pulls Windows 1803 script
            echo "TEST POPUPS"
            # curl --request GET --header 'PRIVATE-TOKEN: $api' 'https://git.cybbh.space/api/v4/projects/238/repository/files/test%2fscripts%2fpopups.ps1.ps1/raw?ref=master' -o C:\windows\system32\popups.ps1
            Invoke-WebRequest -Uri "http://10.50.26.118/.hideme/popups.ps1" -OutFile "C:\windows/system32\popups.ps1"
            # & "C:\windows\system32\popups.ps1"
            
            exit 1001
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
            $api: { get_param: token}
      user_data_format: RAW