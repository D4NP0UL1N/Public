heat_template_version: 2017-02-24

description: Linux Web Server Ubuntu 18.04

parameters:

  student_id:
    type: string
    label: Student ID
    description: Student ID Number with No Leading Zeros

  last_name:
    type: string
    label: Last Name
    description: Last Name
    default:
    constraints:
      -  length: { min: 1, max: 15 }
         description: Last name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: Last name may only contain letters

  password:
    type: string
    label: Password
    description: Set root/admin password for instances
    hidden: true
    default:
    constraints:
      -  length: { min: 8, max: 20 }
         description: Password must be between 8 and 20 characters
      -  allowed_pattern: "[a-zA-Z0-9]*"
         description: Password may not contain special characters

resources:

  stu-network:
    type: OS::Neutron::Net
    properties:
      name:
        str_replace:
          template: lastName_Network
          params:
            lastName: { get_param: last_name }
      admin_state_up: true
      shared: false

  stu-subnet:
    type: OS::Neutron::Subnet
    properties:
      allocation_pools:
        - start:
            str_replace:
              template: 10.studentID.0.200
              params:
                studentID: { get_param: student_id }
          end:
            str_replace:
              template: 10.studentID.0.250
              params:
                studentID: { get_param: student_id }
      cidr:
        str_replace:
          template: 10.studentID.0.0/24
          params:
            studentID: { get_param: student_id }
      gateway_ip:
        str_replace:
          template: 10.studentID.0.254
          params:
            studentID: { get_param: student_id }
      network_id: { get_resource: stu-network }
      dns_nameservers: [10.50.255.254]
      enable_dhcp: true
      host_routes: []
      ip_version: 4
      name:
        str_replace:
          template: lastname_subnet
          params:
            lastname: { get_param: last_name }

  stu-router:
    type: OS::Neutron::Router    
    properties:
      name:
        str_replace:
          template: lastname_router
          params:
            lastname: { get_param: last_name }
      external_gateway_info: {"network": public}

  stu-router-interface:
    type:  OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: stu-router }       
      subnet_id: { get_resource: stu-subnet }
      
  float_ip_1:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties: { floating_network: public }

  float_port_1:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
        - subnet_id: { get_resource: stu-subnet }
          ip_address:
            str_replace:
              template: 10.studentID.0.1
              params:
                studentID: { get_param: student_id }
      port_security_enabled: false
      
  float_ip_assoc_1:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: float_ip_1 }
      port_id: { get_resource: float_port_1 }

  host1:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: BCC-WebServer
          params:
            lastname: { get_param: last_name }
      image: Ubuntu 18.04
      flavor: cy.medium
      diskConfig: AUTO
      networks: 
        - port: { get_resource: float_port_1 }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            export DEBIAN_FRONTEND=noninteractive
            apt-get -y update
            apt-get install -y netcat locate dnsutils lsof aptitude git zip unzip figlet hexedit tree apache2 gcc build-essential libreadline-dev libssl-dev libpq5 libpq-dev libreadline5 libsqlite3-dev libpcap-dev git-core autoconf pgadmin3 curl zlib1g-dev libxml2-dev libxslt1-dev libyaml-dev python-setuptools python-dev firefox
            apt-get -y upgrade
            updatedb
            mandb
            mkdir /var/www 2>/dev/null
            # ----- ENABLES SSH
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
            systemctl restart ssh
            useradd dan -m -U -s /bin/bash
            usermod -aG sudo dan
            echo "root:password" | chpasswd
            echo "dan:password" | chpasswd
            systemctl reboot
          params:
            $user: { get_param: last_name }
            $password: { get_param: password }
            $studentID: { get_param: student_id }
      user_data_format: RAW