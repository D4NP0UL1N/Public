$ErrorActionPreference = 'SilentlyContinue'
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force

#----- Set Password -----
$pass = CovertTo-SecureString -String 'PassWord1234' -AsPlainText -Force

New-Item -itemtype directory -path "C:\windows\system32\transcripts"
Start-Transcript C:\windows\system32\$((get-date).tostring("dd-MMM-yyy_HH.mm.ss")).txt

#----- Enable Local Admin to use internet ---
secedit /export /cfg c:\secpol.cfg
(gc C:\secpol.cfg).replace("MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ScForceOption=4,0", "MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\FilterAdministratorToken=4,1") | Out-File C:\secpol.cfg
secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg
rm -force c:\secpol.cfg -confirm:$false

Write-Output "`nUpdating Help Files ... Please Be Patient`n"
Update-Help -Force
cmd /c "sc config ADWS start= delayed-auto"

#----- Adds cadre account
New-ADOrganizationalUnit -Name CADRE -Path "DC=cyber,DC=warriors"
New-ADUser -Name "Cadre" -Path "OU=CADRE,DC=cyber,DC=warriors" -Description "17C10 Cadre" -AccountPassword $pass -ChangePasswordAtLogon $false -CannotChangePassword $true -PasswordNeverExpires $true -Enabled $true -AllowReversiblePasswordEncryption $false -PassThru
Add-ADGroupMember -Identity "CN=Domain Admins,CN=Users,DC=cyber,DC=warriors" -Members "CN=cadre,OU=CADRE,DC=cyber,DC=warriors" -PassThru

#----- Setup CleanUp Script
Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -name "cleanup" 'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe -noprofile -sta -File "C:\windows\system32\ARC02-L06-3.ps1"'
New-Item $profile.AllUsersAllHosts -ItemType File -Force
Test-Path $profile.AllUsersAllHosts
echo '$ProfileRoot = (Split-Path -Parent $MyInvocation.MyCommand.Path)' > C:\Windows\System32\WindowsPowerShell\v1.0\profile.ps1
echo '$env:path += "$ProfileRoot"' >> C:\Windows\System32\WindowsPowerShell\v1.0\profile.ps1
Restart-Computer